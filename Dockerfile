FROM ubuntu:20.04

WORKDIR /app

COPY . /app

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install cmake && \
    apt-get -y install g++ && \
    apt-get -y install libjpeg-dev && \
    apt-get -y install libpng-dev && \
    apt-get -y install libtiff-dev && \
    apt-get -y install libfftw3-dev && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make

ENV PATH="/app/build/bin:$PATH"

CMD ["bash"]
