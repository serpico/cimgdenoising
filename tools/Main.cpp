/*=============================================================================

_Header

_Module_Name 	: Main.cpp

_Directory	: 

_Description 	: Description of Main.cpp

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 25/10/2018

_end

=============================================================================*/






/*===========================================================================*/


#include "include.h"
#include "define.h"
#include "IOmodules/ByteImage3D.h"
#include "MAD.h"
#include "PEWA.h"
#include "OWF.h"
#include "NLMeans.h"
#include "SAFIR.h"
#include "IOmodules/Sauvegarde.h"
#include "DCTmodules/DCTdenoising.h"
#include "NLBAYESmodules/NlBayes.h"
#include "BM3Dmodules/bm3d.h"

#include "SparseDenoising.hpp"

#include "cimgutils.h"
#include "CImg.h"
#include <iostream>

using namespace cimg_library;

#ifdef cimg_use_visualcpp6
#define std  
#endif

/*===========================================================================*/






/*===========================================================================*/

int main(int argc, char **argv)
{

  // Read command line arguments
  // ---------------------------- 
  cimg_usage("denoise 2D+T images corrupted by Gaussian or Poisson noise : denoise -i InputImage.tif -o outputImage -algo AlgorithmName \n Build : ");

  const char *file_i    = cimg_option("-i",(char*)NULL,"Input image");
  const char *file_o    = cimg_option("-o",(char*)NULL,"Output file");
  int first             = cimg_option("-first",0,"Number of the first image (0: default value)");
  int last              = cimg_option("-last",-1,"Number of the last image (depth or time: default value)\n"); 
  const double alpha    = cimg_option("-alpha",0.,"alpha mixing of input/output images [0. - 1.] (0.: default value)");
  const double scale    = cimg_option("-scale",1.,"Resize the volume in the range [0.5 - 1.5] (1.: default value)");
  double range          = cimg_option("-range",1.,"Automatic intensity scaling (-1) or manual scaling\n");
  const char *algo      = cimg_option("-algo",(char*)NULL,"Algorithm name: ");
  cout << "                                              - BM3D" << endl;
  cout << "                                              - NLBayes" << endl;
  cout << "                                              - NLMeans"<< endl;
  cout << "                                              - BayesNLmeans"<< endl;
  cout << "                                              - SAFIR"<< endl;
  cout << "                                              - PEWA"<< endl;
  cout << "                                              - OWF"<< endl;
  cout << "                                              - DCT"<< endl;
  cout << "                                              - Wiener"<< endl;
  cout << "                                              - Bilateral"<< endl;
  cout << "                                              - Gaussian"<< endl;
  cout << "                                              - Median"<< endl;
  cout << "                                              - TV"<< endl;
  cout << "                                              - SV"<< endl;
  cout << "                                              - HV"<< endl << endl;;
  cout << "    Noise parameters and simulation:" << endl;
  const double noiseg   = cimg_option("-ng",0.0,"Add artificial Gaussian noise before aplying the algorithm");
  const bool noisep     = cimg_option("-np",false,"Add artificial Poisson noise before applying the algorithm"); 
  const double msg      = cimg_option("-msg",0.0,"Adjust manually the assumed Gaussian noise standard deviation");
  const bool stab       = cimg_option("-stab",false,"Variance stabilization for Poisson noise removal\n");
  cout << "    Options and parameters of denoising algorithms:" << endl;
  int patch             = cimg_option("-patch",3,"Half size of the patch (NLMeans, PEWA, OWF, SAFIR, DCT, Wiener) ");
  int neigh             = cimg_option("-neigh",7,"Half size of the neighborhood (NLMeans, PEWA, OWF, SAFIR, DCT, Median, Bilateral) "); 
  float denoisep        = cimg_option("-denoisep",-1.0,"Denoising parameter (NLMeans: 3.5 | DCT: 3.0 | Wiener: 1.25 | Bilateral: 2.0 | Gaussian: 1.0 | TV: 6.0 | SV: 6.0 | HV: 6.0) ");
  float sparsep         = cimg_option("-sparsep",0.6,"Sparsity parameter (SV and HV algorithms) in the range [0.1 - 0.9] ");
  int itersafir = cimg_option("-iter",4,"Number of iterations (NDSafir only)");


  if (!file_i){
      return 0;
  }

  char  *name = new char[100];
  cout <<"\nImage_i : " << file_i << endl;
  cout <<"Image_o : " << file_o << endl;
  sprintf(name,"%s",file_o);
  

  // Algorithm selection
  // -------------------
  char Denoiser = '0';
  if (strcmp(algo,"BM3D")         == 0) Denoiser = '0'; 
  if (strcmp(algo,"NLBayes")      == 0) Denoiser = '1'; 
  if (strcmp(algo,"NLmeans")      == 0) Denoiser = '2'; 
  if (strcmp(algo,"BayesNLmeans") == 0) Denoiser = '3'; 
  if (strcmp(algo,"SAFIR")        == 0) Denoiser = '4'; 
  if (strcmp(algo,"PEWA")         == 0) Denoiser = '5'; 
  if (strcmp(algo,"OWF")          == 0) Denoiser = '6'; 
  if (strcmp(algo,"DCT")          == 0) Denoiser = '7'; 
  if (strcmp(algo,"Wiener")       == 0) Denoiser = '8';
  if (strcmp(algo,"Bilateral")    == 0) Denoiser = '9'; 
  if (strcmp(algo,"Gaussian")     == 0) Denoiser = 'A';  
  if (strcmp(algo,"Median")       == 0) Denoiser = 'B'; 
  if (strcmp(algo,"TV")           == 0) Denoiser = 'C'; 
  if (strcmp(algo,"SV")           == 0) Denoiser = 'D'; 
  if (strcmp(algo,"HV")           == 0) Denoiser = 'E'; 




  // Parameter algorithm by default
  // ------------------------------ 
  if (strcmp(algo,"NLmeans")      == 0 && denoisep == -1.0) denoisep = 3.5;  
  if (strcmp(algo,"DCT")          == 0 && denoisep == -1.0) denoisep = 3.0; 
  if (strcmp(algo,"Wiener")       == 0 && denoisep == -1.0) denoisep = 1.25;
  if (strcmp(algo,"Bilateral")    == 0 && denoisep == -1.0) denoisep = 2.0; 
  if (strcmp(algo,"Gaussian")     == 0 && denoisep == -1.0) denoisep = 1.0; 
  if (strcmp(algo,"TV")           == 0 && denoisep == -1.0) denoisep = 5.0; 
  if (strcmp(algo,"SV")           == 0 && denoisep == -1.0) denoisep = 5.0; 
  if (strcmp(algo,"HV")           == 0 && denoisep == -1.0) denoisep = 5.0; 
 


  // Read image and parameters / Option: add some noise to image
  // -----------------------------------------------------------
  CImg<> src(file_i);
  CImg<> dest(src);

  int w = (int)(scale*src.width());
  int h = (int)(scale*src.height());
  int d = (int)(scale*src.depth());
  if (last<0) last = src.depth();

  float *src_0 = src.data(0,0,0,0);
  float *I0    = new float[w*h];
  for (int ind=0; ind<w*h; ind++) I0[ind] = src_0[ind];
  float *I0seq = new float[w*h*d];
  for (int ind=0; ind<w*h*d; ind++) I0seq[ind] = src_0[ind];

  if (noisep) src.noise(0,3);
  src.noise(noiseg,0);

  //sprintf(name,"%s_Noisy.tif",file_o);
  //src.save(name);
  float *src_i = src.data(0,0,0,0);

  float Max_i = 0.;
  for (int ind=0; ind<w*h*d; ind++) if (src_i[ind]>Max_i)  Max_i = src_i[ind];
  if (range==-1) range = Max_i/255.;  // Intensity normalization
 
  src.resize(w,h,d,1,5); // resize by cubic interpolation - interpolation type (0=none, 1=bloc, 2=mosaic, 3=linear, 4=grid, 5=cubic)
  
  float *IN_i     = new float[w*h];
  float *IN_step1 = new float[w*h];
  float *IN_o     = new float[w*h];
  

 


  // Parameter display
  // -----------------
  cout <<"\nrange     : " << range << endl;
  cout <<"width     : " << w << endl; 
  cout <<"height    : " << h << endl;
  cout <<"depth     : " << d << endl; 
  cout <<"first     : " << first << endl;
  cout <<"last      : " << last << endl; 
  cout <<"algorithm : " << algo <<"("<< Denoiser <<")"<< endl;
  cout <<"parameter : " << denoisep << endl;
  cout <<"stab      : " << stab << endl;
  cout <<"alpha     : " << alpha << endl << endl;




  // Setting or estimation of noise variance 
  // ---------------------------------------
  float sigma  = (float)(noiseg);
  if (msg>0.) sigma = (float)(msg);
  if (msg==-1) sigma = sqrt(src.variance_noise());



   
  // Option: variance stabilisation for Poisson noise removal
  // --------------------------------------------------------
  CImg<float> tga_params, tga_params_;
  
  if (stab){
    cout << "Variance stabilization & statistics" << endl;
    cout << "-----------------------------------" << endl;
    tga_params = estimate_ccd_noise_parameters(src,3.,true,false);
    cout <<"tga_params[0] / e_DC : " <<  tga_params[0] << endl;
    cout <<"tga_params[1] / g0   : " <<  tga_params[1] << endl;
    float g0 = (float)(tga_params[1]);
    float e_DC = (float)(tga_params[0]); 

    if (stab){
      src = stabilize_ccd_noise(src, tga_params);
      cout <<"standard deviation   : " <<  sqrt(src.variance_noise()) << endl;
      sigma  = sqrt(src.variance_noise());
      tga_params_ = estimate_ccd_noise_parameters(src,3.,true,false);
      cout <<"tga_params_[0]       : " <<  tga_params_[0] << endl;
      cout <<"tga_params_[1]       : " <<  tga_params_[1] << endl;
    }

    range=1.;
    cout << "-----------------------------------" << endl << endl;  
  }  


  

  // 3D Stack Denoising
  // ------------------
  cimg::tic();
  float psnr =0.;
  
  for (int z=first; z<last; z++){    

    // ----- Extract a 2D image from the 3D stack ----- //
    float *Idata_i = src.get_shared_slices(z,z).data();   
    for (int ind=0; ind<w*h; ind++){
      I0[ind] = I0seq[ind+(z-first)*w*h];
      IN_i[ind] = (*Idata_i++)/range;
    }
    cout << "PSNR/MSE of the noisy image:" << endl;
    psnr = comp_psnr(I0,w,h,IN_i);
    cout << "Noise standard deviation: " << sigma << endl;


  
    
    // ----- Denoising of a 2D image ----- // 
    switch(Denoiser) {
    case '0' : // BM3D
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "BM3D denoising (source www.ipol.im)" << endl;
      cout << "Dabov et al., IEEE Trans. Image Processing, 16(8):2080-2095, 2007\n" << endl;
      run_bm3d(sigma,IN_i,IN_step1,IN_o,w,h,1,false,true,5,4,0);
      cout<< "> STEP 1: "<<  endl;
      psnr = comp_psnr(I0,w,h,IN_step1);
      cout<< "> STEP 2: "<< endl;
      psnr = comp_psnr(I0,w,h,IN_o);
      cout<< "------------------------------------------------------------------\n" << endl;
      break;
	  
    case '1' : // NLBayes
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "NLBayes denoising (source www.ipol.im)" << endl;
      cout << "Lebrun et al., IPOL, 3:1-42, 2013\n" << endl;
      runNlBayes(IN_i,IN_step1,IN_o,w,h,1,0,sigma,5,100,true);
      cout<< "> STEP 1: "<<  endl;
      psnr = comp_psnr(I0,w,h,IN_step1);
      cout<< "> STEP 2: "<<  endl; 
      psnr = comp_psnr(I0,w,h,IN_o); 
      cout<< "------------------------------------------------------------------\n" << endl;
      break;
	  
    case '2' : // NLmeans denoiser
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "NLmeans denoising" << endl;
      cout << "Buades et al., SIAM MMS, 4(2):490-530, 2005\n" << endl; 
      NLmeans(IN_i,IN_o,w,h,sigma,patch,neigh,denoisep);
      psnr = comp_psnr(I0,w,h,IN_o);
      cout<< "------------------------------------------------------------------\n" << endl;
      break;
	  
    case '3' : // Bayesian NLmeans denoiser
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "BayesNLmeans denoising" << endl;
      cout << "Kervrann et al., in Proc. SSVM'07:520-532, 2007\n" << endl; 
  	  BayesNLmeans(IN_i,IN_o,w,h,sigma,patch,neigh,2);
  	  cout<< "\n"<< endl;
	  psnr = comp_psnr(I0,w,h,IN_o);
	  cout<< "------------------------------------------------------------------\n" << endl;
	  break;
	  
    case '4' : // SAFIR
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "SAFIR denoising " << endl;
      cout << "Kervrann et al., Int. J. Computer Vision, 79(1):45-69, 2008"<< endl;
      cout << "Boulanger et al., IEEE T. Medical Imaging, 29(2):442-454, 2010"<< endl; 
      SAFIR(IN_i,w,h,sigma,patch,itersafir,IN_o);
      psnr = comp_psnr(I0,w,h,IN_o);
      cout<< "------------------------------------------------------------------\n" << endl;
      break;	
	  			
    case '5' : // PEWA denoising
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "PEWA denoising " << endl;
      cout << "Kervrann, Proc. of NIPS, 2014\n"<< endl;
      PEWA(IN_i,IN_i,w,h,sigma,patch,3,IN_step1);
      cout<< "\n > STEP 1: "<<  endl;
      psnr = comp_psnr(I0,w,h,IN_step1);
      PEWA(IN_step1,IN_i,w,h,sigma,patch,neigh,IN_o);
      cout<< "\n > STEP 2: "<< endl;
      psnr = comp_psnr(I0,w,h,IN_o);
      cout<< "------------------------------------------------------------------\n" << endl;
      break;
	  
    case '6' : // OWF denoiser
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "OWF (Optimal Weight Filter) denoising" << endl;
      cout << "Jin et al., SIAM J. Imaging Sciences, 10(4):1878-1920, 2017\n"<< endl;
      OWF(IN_i,IN_i,IN_o,sigma,neigh,patch,w,h);
      psnr = comp_psnr(I0,w,h,IN_o);   
      cout<< "------------------------------------------------------------------\n" << endl;
      break; 
	  
    case '7' : // DCT (Discrete Cosinus Transform) denoiser
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "DCT denoising (source www.ipol.im)" << endl;
      cout << "Yu & Sapiro, IPOL, 292-296, 2011\n"<< endl; 
      DCTdenoising(IN_i,IN_o,w,h,1,denoisep*sigma,2*patch);
      cout << endl;
      psnr = comp_psnr(I0,w,h,IN_o); 
      cout<< "------------------------------------------------------------------\n" << endl;
      break;
	  
    case '8' : // Wiener filtering
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "Wiener filtering\n" << endl;
      WienerFiltering(IN_i,denoisep,sigma,patch,IN_o,w,h);
      psnr = comp_psnr(I0,w,h,IN_o);  
      cout<< "------------------------------------------------------------------\n" << endl;
      break;
	  
   case '9' : // Bilateral filtering
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "Bilateral filtering" << endl;
      cout << "Tomasi & Manduchi, Proc. of ICCV, 839-846, 1998\n"<< endl;
      cout << "Spatial parameter (default: 3): "<< neigh << endl;
      cout << "Range parameter (default: 2.) x sigma : "<< denoisep*sigma << endl;
      src.blur_bilateral(neigh,denoisep*sigma);
      IN_o = src.data(0,0,0,0);
      psnr = comp_psnr(I0,w,h,IN_o);  
      cout<< "------------------------------------------------------------------\n" << endl;
      break;
	  
   case 'A' : // Gaussian filtering
      cout<< "\n-------------------------------------------------------------------" << endl;
      cout << "Gaussian filtering\n " << endl;
      GaussianFiltering(IN_i,denoisep,IN_o,w,h);
      psnr = comp_psnr(I0,w,h,IN_o);  
      cout<< "------------------------------------------------------------------\n" << endl;
      break;	
	  		  
   case 'B' : // Median filtering
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "Median filtering\n " << endl;
      cout << "Size filter (default: 7 x 7): "<< (2*neigh+1) << " x " << (2*neigh+1) << endl;
      src.blur_median(neigh);
      IN_o = src.data(0,0,0,0);
      psnr = comp_psnr(I0,w,h,IN_o); 
      cout<< "------------------------------------------------------------------\n" << endl;
      break;
	  		
   case 'C' : // Sparse Total Variation minimization 
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "Total Variation denoising "  << endl;
      SparseTotalVariationDenoising(IN_i,IN_o,w,h,pow(2,denoisep),1.0,250,Max_i);
      psnr = comp_psnr(I0,w,h,IN_o);  
      cout<< "------------------------------------------------------------------\n" << endl;
      break;   
		 
   case 'D' : // Sparse Total Variation minimization 
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "Sparse Total Variation denoising "  << endl;
      SparseTotalVariationDenoising(IN_i,IN_o,w,h,pow(2,denoisep),sparsep,250,Max_i);
      psnr = comp_psnr(I0,w,h,IN_o);  
      cout<< "------------------------------------------------------------------\n" << endl;
      break;   
	  
   case 'E' : // Sparse Hessian Variation minimization 
      cout<< "\n------------------------------------------------------------------" << endl;
      cout << "Sparse Hessian Variation denoising "  << endl;
      SparseHessianVariationDenoising(IN_i,IN_o,w,h,pow(2,denoisep),sparsep,250,Max_i);
      psnr = comp_psnr(I0,w,h,IN_o);  
      cout<< "------------------------------------------------------------------\n" << endl;
      break;   
	  
   default :
      printf("Invalid denoiser\n" );
      exit(-1);
    }
    

  

    //  ------ Mixing of noisy and denoised images ----- //
    float *Idata_o = dest.get_shared_slices(z,z).data();
    for (int ind=0; ind<w*h; ind++) *Idata_o++ = range*((1.-alpha)*IN_o[ind]+alpha*IN_i[ind]);
    
    cout << "SAVE OF IMAGE #" << z << endl;
    cout << "" << endl;
  }




  // MSE - PSNR computation
  // ---------------------- 
  float MSE = 0.;
  float *dest_0 = dest.data(0,0,0,0);
  for (int ind=0; ind<w*h*d; ind++){
    float val = *dest_0++;
    MSE += (val - I0seq[ind])*(val - I0seq[ind]);
  }
  MSE /= (w*h*d);
  //printf("PSNR = %f db  / RMSEf = %2.4f \n\n", 10*log10(255*255/MSE), sqrt(MSE));

  cout <<  "Image sequence PSNR = "<< 10*log10(255*255/MSE) << "/ RMSE = " << sqrt(MSE) << endl;


 

  // Save
  // ----
  if (stab) dest = stabilize_ccd_noise(dest,tga_params,true);
  char  *name_o = new char[100];
  sprintf(name_o,"%s",file_o);
  cout << "Save of " << name_o << endl;
  dest.save(name_o);


  



  cout <<"\n" << endl;
  cout <<"THE END\n" << endl;
  cimg::toc();

  return 0;
}

/*===========================================================================*/
