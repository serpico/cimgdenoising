### CMakeLists.txt --- 
##
######################################################################
## 
### Commentary: 
## 
######################################################################
## 
### Change log:
## 
######################################################################
enable_testing()

project(cimgdenoisingTest)

enable_testing()

## #################################################################
## Input
## #################################################################

set(${PROJECT_NAME}_MAIN_SOURCES
  ndsafirTest.cpp
  svdenoise2dTest.cpp
)

## ###################################################################
## Configure file
## ###################################################################

if(EXISTS ${PROJECT_SOURCE_DIR}/imagesTest/Montage.pgm)
  set(MONTAGE ${PROJECT_SOURCE_DIR}/imagesTest/Montage.pgm)
endif()

if(EXISTS ${PROJECT_SOURCE_DIR}/imagesTest/ndsafir_result.tif)
  set(NDSAFIRRESULT ${PROJECT_SOURCE_DIR}/imagesTest/ndsafir_result.tif)
endif()

if(EXISTS ${PROJECT_SOURCE_DIR}/imagesTest/sv_result.tif)
  set(SVRESULT ${PROJECT_SOURCE_DIR}/imagesTest/sv_result.tif)
endif()


if(EXISTS ${PROJECT_SOURCE_DIR}/cimgdenoisingTestConfig.h.in)
  configure_file(cimgdenoisingTestConfig.h.in ${CMAKE_BINARY_DIR}/cimgdenoisingTestConfig.h)
endif()


## #################################################################
## Build rules
## #################################################################

SET(TESTS "")

foreach (test ${${PROJECT_NAME}_MAIN_SOURCES})
  get_filename_component(TName ${test} NAME_WE)

  add_executable(${TName} ${test})
  target_link_libraries(${TName}
    cimgdenoising)

  add_test(${TName} ${EXECUTABLE_OUTPUT_PATH}/${TName})

  set(TEST ${TESTS} ${EXECUTABLE_OUTPUT_PATH}/${TName})
endforeach()

add_custom_target(testcimgdenoising COMMAND ${CMAKE_CTEST_COMMAND} DEPENDS ${TESTS})

## #################################################################
## Source file layout in development environments like Visual Studio
## #################################################################
SOURCE_GROUP("Header Files" REGULAR_EXPRESSION .*\\.h\$)
SOURCE_GROUP("Source Files" REGULAR_EXPRESSION .*\\.cpp\$)
