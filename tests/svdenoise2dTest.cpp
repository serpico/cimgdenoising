#include <cimgdenoising>
#include "cimgdenoisingTestConfig.h"

#include <iostream>

int main(int argc, char *argv[])
{

    std::cout << "input image = " << MONTAGE << std::endl;
    std::cout << "result image = " << SVRESULT << std::endl;

    // I/O images
    CImg<float> src(MONTAGE);

    double scale = 1.;
    int w = (int)(scale*src.width());
    int h = (int)(scale*src.height());
    float *IN_i     = new float[w*h];
    float *IN_o     = new float[w*h];

    float *src_0 = src.data(0,0,0,0);
    for (int ind=0; ind<w*h; ind++) IN_i[ind] = src_0[ind];

    float Max_i = 0.;
    for (int ind=0; ind<w*h; ind++) if (IN_i[ind]>Max_i)  Max_i = IN_i[ind];

    // parameters
    float denoisep = 7;
    float sparsep = 0.6;

    // run
    SparseTotalVariationDenoising(IN_i,IN_o,w,h,pow(2,denoisep),sparsep,250,Max_i);

    // save output image for the first test
    CImg<float> dest = CImg<float>(IN_o, w, h,1);

    // calculate error with the reference image
    cimg_library::CImg<double> resultImage(SVRESULT);
    double error = resultImage.MSE(dest);
    if (error > 10)
    {
        return 1;
    }
    return 0;

}
