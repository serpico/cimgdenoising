#!/bin/bash

echo ----------------------
echo Install CimgDenoising
echo ----------------------

# install lib dependencies
echo "install CImg libs"
echo "-----------------"
apt-get install libpng-dev
apt-get install libtiff-dev
apt-get install libfftw3-dev

# install compilation dependencies
apt-get install cmake

# make cimgdenoising
echo Install CImgDenoising
echo ----------------------
git clone git@gitlab.inria.fr:serpico/cimgdenoising.git
cd cimgdenoising
mkdir build
cd build
cmake ..
make

# run tests
cd tests
ctest
