var m_sparsep;
var m_denoisep;

macro "Denoising_SV"{ 

	Dialog.create("Denoising_SV");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "Serpico" + File.separator + "DenoisingJ" + File.separator + "tutorial" + File.separator + "index.html#SV");
	Dialog.addMessage("Sparse Total Variation denoising");
	Dialog.addNumber("Sparsity parameter: ", 0.6);
	Dialog.addNumber("Denoising parameter: ", 6.0);
	Dialog.show();

	m_sparsep = Dialog.getNumber();
	m_denoisep = Dialog.getNumber();

 
	process(); 

} 

function process(){

	// Save the input images in tmp
	tmpPath = getDirectory("temp"); 
	imageTitle = getTitle(); 
	saveAs("tiff", tmpPath + "tmp.tif"); 
	rename(imageTitle);

	// Run cmd
	execDir = getDirectory("plugins") + "bin" + File.separator; 
	execPath = getDirectory("plugins") + "bin" + File.separator + "denoise"; 
	exec(execPath , "-sparsep", m_sparsep, "-denoisep", m_denoisep, "-o", tmpPath + "o.tif", "-algo", "SV", "-i", tmpPath + "tmp.tif");

	// Load outputs
	open(tmpPath + "o.tif_Denoised.tif");
	File.delete(tmpPath + "o.tif_Denoised.tif");

}
