var m_patch;
var m_neigh;

macro "Denoising_PEWA"{ 

	Dialog.create("Denoising_PEWA");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "Serpico" + File.separator + "DenoisingJ" + File.separator + "tutorial" + File.separator + "index.html#PEWA");
	Dialog.addMessage("PEWA denoising Kervrann, Proc. of NIPS, 2014\n");
	Dialog.addNumber("Half size of the patch: ", 3);
	Dialog.addNumber("Half size of the neighborhood: ", 7);
	Dialog.show();

	m_patch = Dialog.getNumber();
	m_neigh = Dialog.getNumber();

 
	process(); 

} 

function process(){

	// Save the input images in tmp
	tmpPath = getDirectory("temp"); 
	imageTitle = getTitle(); 
	saveAs("tiff", tmpPath + "tmp.tif"); 
	rename(imageTitle);

	// Run cmd
	execDir = getDirectory("plugins") + "bin" + File.separator; 
	execPath = getDirectory("plugins") + "bin" + File.separator + "denoise"; 
	exec(execPath , "-patch", m_patch, "-neigh", m_neigh, "-o", tmpPath + "o.tif", "-algo", "PEWA", "-i", tmpPath + "tmp.tif");

	// Load outputs
	open(tmpPath + "o.tif_Denoised.tif");
	File.delete(tmpPath + "o.tif_Denoised.tif");

}
