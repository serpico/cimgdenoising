var m_msg;

macro "Denoising_BM3D"{ 

	Dialog.create("Denoising_BM3D");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "Serpico" + File.separator + "DenoisingJ" + File.separator + "tutorial" + File.separator + "index.html#BM3D");
	Dialog.addMessage("Image denoising using BM3D method from \n Dabov et al., IEEE Trans. Image Processing, 16(8):2080-2095, 2007");
	Dialog.addNumber("Gaussian noise standard deviation: ", 20.0);
	Dialog.show();

	m_msg = Dialog.getNumber();

 
	process(); 

} 

function process(){

	// Save the input images in tmp
	tmpPath = getDirectory("temp"); 
	imageTitle = getTitle(); 
	saveAs("tiff", tmpPath + "tmp.tif"); 
	rename(imageTitle);

	// Run cmd
	execDir = getDirectory("plugins") + "bin" + File.separator; 
	execPath = getDirectory("plugins") + "bin" + File.separator + "denoise"; 
	exec(execPath , "-msg", m_msg, "-o", tmpPath + "o.tif", "-algo", "BM3D", "-i", tmpPath + "tmp.tif");

	// Load outputs
	open(tmpPath + "o.tif_Denoised.tif");
	File.delete(tmpPath + "o.tif_Denoised.tif");

}
