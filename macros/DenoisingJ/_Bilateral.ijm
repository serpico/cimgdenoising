var m_neigh;
var m_denoisep;

macro "Denoising_Bilateral"{ 

	Dialog.create("Denoising_Bilateral");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "Serpico" + File.separator + "DenoisingJ" + File.separator + "tutorial" + File.separator + "index.html#Bilateral");
	Dialog.addMessage("Bilateral filtering \n Tomasi, Manduchi, Proc. of ICCV, 839-846, 1998\n");
	Dialog.addNumber("Half size of the neighborhood: ", 7);
	Dialog.addNumber("Denoising parameter: ", 2.0);
	Dialog.show();

	m_neigh = Dialog.getNumber();
	m_denoisep = Dialog.getNumber();

 
	process(); 

} 

function process(){

	// Save the input images in tmp
	tmpPath = getDirectory("temp"); 
	imageTitle = getTitle(); 
	saveAs("tiff", tmpPath + "tmp.tif"); 
	rename(imageTitle);

	// Run cmd
	execDir = getDirectory("plugins") + "bin" + File.separator; 
	execPath = getDirectory("plugins") + "bin" + File.separator + "denoise"; 
	exec(execPath , "-neigh", m_neigh, "-denoisep", m_denoisep, "-o", tmpPath + "o.tif", "-algo", "Bilateral", "-i", tmpPath + "tmp.tif");

	// Load outputs
	open(tmpPath + "o.tif_Denoised.tif");
	File.delete(tmpPath + "o.tif_Denoised.tif");

}
