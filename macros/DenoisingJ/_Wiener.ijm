var m_patch;
var m_denoisep;

macro "Denoising_Wiener"{ 

	Dialog.create("Denoising_Wiener");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "Serpico" + File.separator + "DenoisingJ" + File.separator + "tutorial" + File.separator + "index.html#Wiener");
	Dialog.addMessage("Wiener filtering");
	Dialog.addNumber("Half size of the patch: ", 3);
	Dialog.addNumber("Denoising parameter: ", 1.25);
	Dialog.show();

	m_patch = Dialog.getNumber();
	m_denoisep = Dialog.getNumber();

 
	process(); 

} 

function process(){

	// Save the input images in tmp
	tmpPath = getDirectory("temp"); 
	imageTitle = getTitle(); 
	saveAs("tiff", tmpPath + "tmp.tif"); 
	rename(imageTitle);

	// Run cmd
	execDir = getDirectory("plugins") + "bin" + File.separator; 
	execPath = getDirectory("plugins") + "bin" + File.separator + "denoise"; 
	exec(execPath , "-patch", m_patch, "-denoisep", m_denoisep, "-o", tmpPath + "o.tif", "-algo", "Wiener", "-i", tmpPath + "tmp.tif");

	// Load outputs
	open(tmpPath + "o.tif_Denoised.tif");
	File.delete(tmpPath + "o.tif_Denoised.tif");

}
