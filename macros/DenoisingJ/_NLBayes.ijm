var m_msg;

macro "Denoising_NLBayes"{ 

	Dialog.create("Denoising_NLBayes");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "Serpico" + File.separator + "DenoisingJ" + File.separator + "tutorial" + File.separator + "index.html#NLBayes");
	Dialog.addMessage("NLBayes denoising (source www.ipol.im) \n Lebrun et al., IPOL, 3:1-42, 2013");
	Dialog.addNumber("Gaussian noise standard deviation: ", 20.0);
	Dialog.show();

	m_msg = Dialog.getNumber();

 
	process(); 

} 

function process(){

	// Save the input images in tmp
	tmpPath = getDirectory("temp"); 
	imageTitle = getTitle(); 
	saveAs("tiff", tmpPath + "tmp.tif"); 
	rename(imageTitle);

	// Run cmd
	execDir = getDirectory("plugins") + "bin" + File.separator; 
	execPath = getDirectory("plugins") + "bin" + File.separator + "denoise"; 
	exec(execPath , "-msg", m_msg, "-o", tmpPath + "o.tif", "-algo", "NLBayes", "-i", tmpPath + "tmp.tif");

	// Load outputs
	open(tmpPath + "o.tif_Denoised.tif");
	File.delete(tmpPath + "o.tif_Denoised.tif");

}
