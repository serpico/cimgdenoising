var m_patch;
var m_neigh;
var m_denoisep;

macro "Denoising_DCT"{ 

	Dialog.create("Denoising_DCT");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "Serpico" + File.separator + "DenoisingJ" + File.separator + "tutorial" + File.separator + "index.html#DCT");
	Dialog.addMessage("DCT denoising (source www.ipol.im) Yu, Sapiro, IPOL, 292-296, 2011\n");
	Dialog.addNumber("Half size of the patch: ", 3);
	Dialog.addNumber("Half size of the neighborhood: ", 7);
	Dialog.addNumber("Denoising parameter: ", 3.0);
	Dialog.show();

	m_patch = Dialog.getNumber();
	m_neigh = Dialog.getNumber();
	m_denoisep = Dialog.getNumber();

 
	process(); 

} 

function process(){

	// Save the input images in tmp
	tmpPath = getDirectory("temp"); 
	imageTitle = getTitle(); 
	saveAs("tiff", tmpPath + "tmp.tif"); 
	rename(imageTitle);

	// Run cmd
	execDir = getDirectory("plugins") + "bin" + File.separator; 
	execPath = getDirectory("plugins") + "bin" + File.separator + "denoise"; 
	exec(execPath , "-patch", m_patch, "-neigh", m_neigh, "-denoisep", m_denoisep, "-o", tmpPath + "o.tif", "-algo", "DCT", "-i", tmpPath + "tmp.tif");

	// Load outputs
	open(tmpPath + "o.tif_Denoised.tif");
	File.delete(tmpPath + "o.tif_Denoised.tif");

}
