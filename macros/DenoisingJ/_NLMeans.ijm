var m_patch;
var m_neigh;
var m_denoisep;

macro "Denoising_NLMeans"{ 

	Dialog.create("Denoising_NLMeans");
	Dialog.addHelp("file:///" + getDirectory("plugins") + "Serpico" + File.separator + "DenoisingJ" + File.separator + "tutorial" + File.separator + "index.html#NLMeans");
	Dialog.addMessage("NLmeans denoising \n Buades et al., SIAM MMS, 4(2):490-530, 2005 \n");
	Dialog.addNumber("Half size of the patch: ", 3);
	Dialog.addNumber("Half size of the neighborhood: ", 7);
	Dialog.addNumber("Denoising parameter: ", 3.5);
	Dialog.show();

	m_patch = Dialog.getNumber();
	m_neigh = Dialog.getNumber();
	m_denoisep = Dialog.getNumber();

 
	process(); 

} 

function process(){

	// Save the input images in tmp
	tmpPath = getDirectory("temp"); 
	imageTitle = getTitle(); 
	saveAs("tiff", tmpPath + "tmp.tif"); 
	rename(imageTitle);

	// Run cmd
	execDir = getDirectory("plugins") + "bin" + File.separator; 
	execPath = getDirectory("plugins") + "bin" + File.separator + "denoise"; 
	exec(execPath , "-patch", m_patch, "-neigh", m_neigh, "-denoisep", m_denoisep, "-o", tmpPath + "o.tif", "-algo", "NLMeans", "-i", tmpPath + "tmp.tif");

	// Load outputs
	open(tmpPath + "o.tif_Denoised.tif");
	File.delete(tmpPath + "o.tif_Denoised.tif");

}
