/*=============================================================================

   copyright(c) 2003-2004 by INRA-Biometrie Jouy-en-Josas.
	                  All Rights Reserved.

-------------------------------------------------------------------------------

_Header

_Module_Name 	: SAFIR.cc

_Directory	: 

_Description 	: Description de SAFIR.cpp

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 29/10/18

_end

=============================================================================*/




 

/*===========================================================================*/

#include "include.h"
#include "define.h"
#include "MAD.h"
#include "IOmodules/ByteImage3D.h"
#include "IOmodules/Sauvegarde.h"

#include "CImg.h"

/*===========================================================================*/







/*=============================================================================

-------------------------------------------------------------------------------
_Function_name 	: table_chi2
_Subject  	:

-----------------------------------------------------------------------------*/

float table_chi2(float p, int dof)
{
  int    k=0;
  double tchi2;
  double Pr[7] = {0.8, 0.9, 0.95, 0.975, 0.99, 0.995, 0.999};
  double chi2[9][7] = { {1.64, 2.71, 3.84, 5.02, 6.64, 7.88, 10.83},
			{0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00},
			{12.24, 14.68, 16.92, 19.02, 21.67, 23.59, 27.88},
			{0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00},
			{30.68, 34.38, 37.65, 40.65, 44.31, 49.93, 52.62},
			{42.88, 47.21, 51.00, 54.44, 58.62, 61.58, 67.99},
			{57.08, 62.04, 66.34, 70.22, 74.92, 78.32, 85.35},
			{73.28, 78.86, 83.68, 88.00, 93.22, 96.88, 104.72},
			{91.47, 97.68, 103.01, 107.78, 113.51, 117.52, 126.08} };

  if (p<0.8) {
    //std::cout << "re-preciser la p-value  : " << std::endl;
    exit(-1);
  }
  else{
    p = MIN (0.999, MAX (0.800, p));
    do k++; while (p-Pr[k]>=0 && k<6);
    tchi2 = ((p-Pr[k-1])*chi2[dof][k]-(p-Pr[k])*chi2[dof][k-1])
      /(Pr[k]-Pr[k-1]);
  }
  return (tchi2);
}

/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Conditions d'Arret
_Subject  :

-----------------------------------------------------------------------------*/

float moyenne(float *X, int N)
{
  float m = 0.;

  for (int i=0; i<N; i++)
     m += X[i];

  return(m/(N));
}


float variance(float *X, int N)
{
  float v = 0.;
  float meanX = moyenne(X,N);

  for (int i=0; i<N; i++)
      v += (X[i]-meanX)*(X[i]-meanX);

  return(v/(N));

}


float correlation(float *X, float *Y, int N)
{
  float c = 0.;
  float meanX = moyenne(X,N);
  float varX  = variance(X,N);
  float meanY = moyenne(Y,N);
  float varY  = variance(Y,N);

  for (int i=0; i<N; i++)
      c +=  (X[i]-meanX)*(Y[i]-meanY);

  c /= (N*sqrt(varX*varY));

  return(c);
}


int  condition_total_exactitude(float *X, float *Y, int N)
{
  int booleen = 1;
  int i;

  for(i=0; i<N; i++)
    if (ABSXY(X[i],Y[i])>0)
      booleen = 0;

  return(booleen);
}


int  condition_correlation(float *I, float *f, int N, float *c_)
{
  int booleen;
  int i;
  float c;
  float *noise = new float[N];

  for(i=0; i<N; i++)
    noise[i] = I[i] - f[i];

  c = correlation(noise,f,N);
  booleen = (ABSX(c)>ABSX(*c_));

  //std::cout<<" -> Correlation (image, bruit) =  "<< ABSX(c) <<"\n";
  *c_ = c;

  delete[] noise;

  return(booleen);
}


float  I_divergence(float *f, float *f_, int N)
{
  int i;
  float I_div = 0.;

  for(i=0; i<N; i++)
    I_div += f[i]*log((f[i]+0.00001)/(f_[i]+0.00001))-f[i]+f_[i];

  return(I_div);
}

/*=============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : initialisation_patches
_Subject  :

-----------------------------------------------------------------------------*/

void initialisation_patches(float *_pixmap, float sig, float *sig_I, float *f_set,
		    float *Var_set, int w, int h)
{
  int i, j, ind;

  // Taille du voisinage initial: |U0|=1
  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      ind=i+w*j;
      f_set[ind]   = _pixmap[ind];
      Var_set[ind] = sig_I[ind];
    }
}

/*=============================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : SAFIR
_Subject  :

-----------------------------------------------------------------------------*/

void  SAFIR(float *_pixmap, int w, int h, float SigmaManuel, int TaillePatches, int ITER, float *f_star)

{
  char  TestArret[100] = "I-div";
  char  IndexPics[100] = "P"; 
  char  IndexFast[100] = "FAST";
  // int   ITER = 4;
  int   TailleMemoire = ITER+2;
  int   dPatches = 1;
  float VarMinEstimateur = 0.;
  float eta = 2*sqrt(2);
  float lambda = 0.99; 

  int   i_, j_, i, j, k, l, pt, t;
  int   iter = 0;
  int   delta = 0;
  int   DELTA;
  int   iter_;
  int   tm = 0;
  int   Npts = 0;
  int   booleen = 0;
  int   ind, ind_w;
  float f, var;
  float sum_weight;
  float weight;
  float card;
  float corr_ = 1;
  float sig;
  float n;





  /* Allocation dynamique */
  /* ==================== */
  int   *H_window = new int[(ITER+1)];
  float *sig_I    = new float[w*h];
  float *residuG  = new float[w*h];
  float *Var_iter = new float[w*h];
  float *f_iter   = new float[w*h];
  float *w_iter   = new float[w*h];
  float *c_iter   = new float[w*h];
  float *n_iter   = new float[w*h];
  float *Var_set  = new float[TailleMemoire*w*h];
  float *f_set    = new float[TailleMemoire*w*h];
  float *Var_star = new float[w*h];
  float *w_star   = new float[w*h];
  float *c_star   = new float[w*h];
  float *n_star   = new float[w*h];
  float *I_div    = new float[(ITER+1)];





  /* Calcul d'une approximation Gaussienne */
  /* ===================================== */
  int    N       = 1000;
  double max_exp = 10;
  double *gauss  = new double[N];

  for (int k=0; k<N; k++){
    double x = (double)k/(double)N*max_exp;
    gauss[k] = exp(-x/2.);
   }
 




  /* Etapes d'initialisation de l'agorithme */
  /* ====================================== */
  //std::cout << " " << std::endl;
  //std::cout << "-----------------------" << std::endl;
  //std::cout << "Etapes d'initialisation" << std::endl;
  //std::cout << "-----------------------" << std::endl;
  
  printf("\nNumber of iterations: %d\n", ITER);
  printf("Patch size: %d x %d\n", 2*TaillePatches+1, 2*TaillePatches+1); 
  if (lambda<1.){
    int dof = (int)(2*(float)(TaillePatches)/dPatches);
    lambda = table_chi2(lambda,dof);
    printf("Chi-2 smoothing parameter (p-value = 0.99): %2.2f \n", lambda);
  }

 


  if (*IndexFast=='F')
      DELTA = (int)(pow(2.,ITER-1));
    else
      DELTA = ITER;

  PseudoResidu(_pixmap,residuG,w,h);
  sig = SigmaLTS(residuG,w*h);
  if (SigmaManuel>1.)
    sig = SigmaManuel;
  //std::cout << "sigma robuste  : " << sig << std::endl;

  //std::cout << "variance minimale imposee de l'estimateur  : " << VarMinEstimateur << std::endl;


  float Hresidus=0.;
  for (ind=0; ind<w*h; ind++)
    if (residuG[ind]<sig)
      Hresidus+=1.;
  float eta_max = sqrt(2*log((ITER*(ITER-1))/(1-Hresidus/(w*h))));
  //std::cout << "\nProbabilite empirique d'un point de discontinuite : " << 1.-Hresidus/(w*h) << std::endl;
  eta = eta_max;
  //std::cout << "eta < " << eta << std::endl;
  printf("Local stopping threshold (eta): %2.2f\n\n", eta);
  sig = sig*sig;

  for (ind=0; ind<w*h; ind++) sig_I[ind] = sig;
  initialisation_patches(_pixmap,sig,sig_I,f_set,Var_set,w,h);





  /* Estimation non-parametrique adaptative */
  /* ====================================== */
  //std::cout << "\n " << std::endl;
  //std::cout << "--------------------------------------" << std::endl;
  //std::cout << "Estimation non-parametrique adaptative" << std::endl;
  //std::cout << "--------------------------------------" << std::endl;

  if (*IndexPics=='P')
    iter_=0;
  else
    iter_=1;

  I_div[0] = 1;
  unsigned char* carte_iter = new unsigned char[w*h];
  for(ind=0; ind<w*h; ind++){
    carte_iter[ind] = ITER;
  }

  iter=0;
  while((iter<ITER) && (booleen==0)){
    iter++;
    if (*IndexFast=='F')
      delta = (int)(pow(2.,iter-1));
    else
      delta = iter;



    //std::cout << "\nIteration No " <<  iter << std::endl;
    //std::cout << "1/2 taille de la fenetre " <<  delta << std::endl;

    if (iter%TailleMemoire==0) tm = 0;
    pt = tm*(w*h);
    tm++;


    /* Sauvegarde des estimations successives */
    /* -------------------------------------- */
    for(ind=0; ind<w*h; ind++){
      f_iter[ind]    = f_set[pt+ind];
      Var_iter[ind]  = Var_set[pt+ind];
      w_iter[ind]    = w_star[ind];
      c_iter[ind]    = c_star[ind];
      n_iter[ind]    = n_star[ind];
    }



    /* Estimation adaptative */
    /* --------------------- */
    int du = TaillePatches;
    int dv = TaillePatches;
    float wmax = 0.;
    float Var_M = 0.;
    float Var_m = 255.;

    for(i=0; i<w; i++)
      for(j=0; j<h; j++){
	ind=i+w*j;
	if(carte_iter[ind]==ITER)
	{
	    sum_weight = 0;
	    f          = 0;
	    var        = 0;
	    n          = 0.;
	    card       = 0.;

	    for (k=-delta; k<=delta; k++)
	      for (l=-delta; l<=delta; l++){
		i_ = i+k;
		j_ = j+l;

		float cout = 0.;

                if (INCLUS(i_,j_,w,h)!=0){
		  ind_w=i_+w*j_;

		  for (int u=-du; u<=du; u+=dPatches)
		    for (int v=-dv; v<=dv; v+=dPatches){
		      int p  = i+u;
		      int q  = j+v;
		      int p_ = i_+u;
		      int q_ = j_+v;

		      if ((INCLUS(p,q,w,h)!=0) && (INCLUS(p_,q_,w,h)!=0)){
			int pq = p+w*q;
			int p_q_ = p_+w*q_;

			// Distance de Mahalanobis symetrique
			float dist2 = (f_iter[pq]-f_iter[p_q_])*(f_iter[pq]-f_iter[p_q_]);
			cout += (dist2/Var_iter[pq] + dist2/Var_iter[p_q_])/2;
		      }
		    }

		  cout /= lambda;
		  weight = exp(-cout/2);

		  if (cout<=1)   card++;
		  f          += weight*_pixmap[ind_w];
		  var        += SQR(weight)*sig_I[ind_w];
		  sum_weight += weight;
		  n++;
		}
	      }

	    f_star[ind]   = f/sum_weight;
	    Var_star[ind] = var/(sum_weight*sum_weight);
	    w_star[ind]   = sum_weight/n;
	    c_star[ind]   = card/n;
	    n_star[ind]   = n;

	    if (Var_star[ind] > Var_M) Var_M = Var_star[ind];
	    if (Var_star[ind] < Var_m) Var_m = Var_star[ind];

	    Var_star[ind] = MAX(Var_star[ind],VarMinEstimateur);
	}
      }

    //std::cout << "Variance minimale de l'estimateur : " << Var_m << std::endl;
    //std::cout << "Variance maximale de l'estimateur : " << Var_M << std::endl;



    /* Etape de controle */
    /* ----------------- */
    for(ind=0; ind<w*h; ind++){
      if (iter>iter_+1)
	if(carte_iter[ind]==ITER)
	{
	  t    = 1;
	  f    = f_set[ind+w*h];
	  var  = Var_set[ind+w*h];
	  while((t<tm) && carte_iter[ind]==ITER){
	    t++;
	    if (ABSXY(f_star[ind],f)>eta*sqrt(var)){
	      f_star[ind]       = f_iter[ind];
	      Var_star[ind]     = Var_iter[ind];
	      w_star[ind]       = w_iter[ind];
	      c_star[ind]       = c_iter[ind];
	      n_star[ind]       = n_iter[ind];
	      carte_iter[ind]   = iter-1;
	      H_window[iter-1] += 1;
	      Npts++;
	    }
	    else{
	      f   = f_set[ind+t*w*h];
	      var = Var_set[ind+t*w*h];
	    }
	  }
	}
    }

    if (iter%(TailleMemoire-1)==0) tm = 0;
    pt = tm*(w*h);

    for(ind=0; ind<w*h; ind++){
      f_set[pt+ind]   = f_star[ind];
      Var_set[pt+ind] = Var_star[ind];
    }



    /* Test d'arret */
    /* ------------ */
    if (!strcmp(TestArret,"corr"))
      booleen = condition_correlation(_pixmap,f_star,w*h,&corr_);
    else
      if (!strcmp(TestArret,"I-div")){
	if (iter>1)
	  I_div[iter-1] = I_divergence(f_star,f_iter,w*h);

	I_div[0] = MAX(I_div[0],I_div[1]);

	if  (iter>1){
	  I_div[iter-1] /= I_div[0];
	  //std::cout<<" -> I-divergence =  " << I_div[iter-1] << std::endl;
	  booleen = 0;
	  if (I_div[iter-1]<0.02) booleen = 1;
	}
      }
      else
	booleen = condition_total_exactitude(f_star, f_iter, w*h);

    if (booleen==1) {
      for(ind=0; ind<w*h; ind++)
	if (carte_iter[ind] == ITER)
	  carte_iter[ind] = iter;
      ITER = iter;
    }
  }

  //std::cout << " " << std::endl;
  // cout << "Nombre d'iterations : " << iter << std::endl;
  //std::cout << "Nombre de points verifiant le test : " << Npts << std::endl;
  //std::cout << "\n " << std::endl;

  //std::cout << "------------------------------------- " << std::endl;
  //std::cout << "Histogramme de la taille des fenetres " << std::endl;
  //std::cout << "------------------------------------- " << std::endl;
  //std::cout << " " << std::endl;

  int sum_H_window=0;
  for (iter=0; iter<ITER; iter++){
    //std::cout << "H[" << iter << "] = " << H_window[iter] << std::endl;
    sum_H_window += H_window[iter];
  }

  //std::cout << "H[" << ITER << "] = " << w*h - sum_H_window << std::endl;
  //std::cout << "\nProbabilite iter < ITER  : " << float(sum_H_window)/(w*h) << std::endl;






  /* Delete */
  /* ====== */
  delete[] sig_I;
  delete[] Var_iter;
  delete[] f_iter;
  delete[] w_iter;
  delete[] c_iter;
  delete[] n_iter;
  delete[] Var_set;
  delete[] f_set;
  delete[] Var_star;
  delete[] w_star;
  delete[] c_star;
  delete[] n_star;
  delete[] I_div;
  delete[] H_window;
  delete[] gauss;



}

/*=============================================================================*/





