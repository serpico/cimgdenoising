/*=============================================================================

_Header

_Module_Name 	: SparseDenoising.hpp

_Directory	: 

_Description 	: Description de SparseDenoising.hpp

_Calls   	:

_Authors	: Hoai-Nam Nguyen

_Contents       :

_Last Release   : 26/09/18

_end

=============================================================================*/




 
 
 
/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : SparseTotalVariationDenoising 
_Subject  :

-----------------------------------------------------------------------------*/

void SparseTotalVariationDenoising(float *IN_i, float *IN_o, int w, int h, const double regularization_parameter,
										const double weighting_parameter, const int nb_iterations, float Max_i)  

{
	printf("\nRegularization parameter (default: 100) : %2.2f\n", regularization_parameter);
  	printf("Sparsity parameter (default: 0.6 in [0.1 - 0.9]: %2.2f \n", weighting_parameter);
  	printf("Maximum number of iterations: %d\n", nb_iterations);
  
  	bool verbose = false;
  	double normalized_L2_error_threshold = 1e-6;

	//float alpha = 0.0;  
  	float Average_IN_i= 0.;
	for (int ind=0; ind<w*h; ind++) Average_IN_i += IN_i[ind];
	Average_IN_i /= (w*h);
	cout << "Average_intensity = "<< Average_IN_i << endl;

		
  	CImg<double> noisy_image(w,h);
  	double *ni = noisy_image.data(0,0,0,0);
  	for (int ind=0; ind<w*h; ind++) *ni++ = (double)(*IN_i++);
	  
  	int img_width = noisy_image.width();
  	int img_height = noisy_image.height();

  	// Splitting parameters
  	double dual_step = MAX(0.01, MIN(0.1, regularization_parameter));
	
  	double primal_step = 0.99 / (0.5 + (8 * pow(weighting_parameter, 2.)
				   			+ pow(1 - weighting_parameter, 2.)) * dual_step);

	//double primal_step = 0.99 / (0.5 * (1. + alpha) + (8 * pow(weighting_parameter, 2.)
	//							+ pow(1 - weighting_parameter, 2.)) * dual_step); //MODIFIED
	
  	cout << "Eta = " << primal_step << "; Nu = " << dual_step << endl;
  	double primal_weight = primal_step * weighting_parameter;
  	double primal_weight_comp = primal_step * (1 - weighting_parameter);
  	double dual_weight = dual_step * weighting_parameter;
  	double dual_weight_comp = dual_step * (1 - weighting_parameter);
  
  	// Initializations
  	CImg<double> denoised_image(noisy_image);
  	CImgList<double> dual_images(3, img_width, img_height, 1, 1, 0.);
  	CImg<double> auxiliary_image;
  
  	// Denoising process
  	double img_L2_norm = sqrt(noisy_image.get_sqr().sum());
  	double tmp, dx, dy, min_val=0., max_val=(double)(Max_i), dx_adj, dy_adj;

  	for (int iter = 0; iter < nb_iterations; iter++) { 
		// Primal optimization
		auxiliary_image = denoised_image;
		
	  	float Average_IN_o = 0.;
		for (int ind=0; ind<w*h; ind++) Average_IN_o += denoised_image[ind];
		Average_IN_o /= (w*h);
		
    
    	cimg_forXY(denoised_image, x, y){
			tmp = denoised_image(x, y)
	  	  	  	- primal_step * (denoised_image(x, y) - noisy_image(x, y));
		

			if (x > 0){
				dx_adj = dual_images[0](x - 1, y) - dual_images[0](x, y);
			}
			else{
				if (y > 0){
					dx_adj = 0.0;
				}
			}
			if (y > 0){
	  	  	    dy_adj = dual_images[1](x, y - 1) - dual_images[1](x, y);
			}
			else{
				if (x > 0){
					dy_adj = 0.0;
				}
	  	  	    // dy_adj = dual_images[1](x, y);
			}
			if (x == 0 && y == 0){
				dy_adj = 0.0;
			}
			
	
			tmp -= (primal_weight * (dx_adj + dy_adj) + primal_weight_comp * dual_images[2](x, y));
			denoised_image(x, y) = MAX(min_val, MIN(max_val, tmp));
    	}

    	// Stopping criterion
    	double error = sqrt((denoised_image - auxiliary_image).get_sqr().sum()) / img_L2_norm;
   	 	
		if (verbose)
     		 if (iter % MAX(1, nb_iterations / 10) == 0)
				 cout << "Iteration #" << iter << " ; Error = " << error
	     	 << "; Threshold = " << normalized_L2_error_threshold
	     	 << endl; //debug
    
		if (iter>1 && error < normalized_L2_error_threshold){
      	  cout <<"Number of actual iterations: " <<  iter << endl;
      	  break;
    	}
    
   	 	// Dual optimization
    	cimg_forXY(auxiliary_image, x, y)
      	  auxiliary_image(x, y) = 2 * denoised_image(x, y) - auxiliary_image(x, y);

    	cimg_forXY(auxiliary_image, x, y) {
      	  if (x < img_width - 1) {
			  dx = auxiliary_image(x + 1, y) - auxiliary_image(x, y);
			  dual_images[0](x, y) += dual_weight * dx;
      		}
      	  if (y < img_height - 1) {
			  dy = auxiliary_image(x, y + 1) - auxiliary_image(x, y);
			  dual_images[1](x, y) += dual_weight * dy;
      		}

      	  dual_images[2](x, y) += dual_weight_comp * auxiliary_image(x, y);
    	}

    	cimg_forXY(auxiliary_image, x, y) {
      	  double tmp = MAX(1., 1. / regularization_parameter * sqrt(pow(dual_images[0](x, y), 2.)
				   		+ pow(dual_images[1](x, y), 2.) + pow(dual_images[2](x, y), 2.)));
          dual_images[0](x, y) /= tmp;
      	  dual_images[1](x, y) /= tmp;
      	  dual_images[2](x, y) /= tmp;
    	}
  	} // endfor (int iter = 0; iter < nb_iters_max; iter++)

  //return denoised_image;
  double *di = denoised_image.data(0,0,0,0);
  for (int ind=0; ind<w*h; ind++) IN_o[ind] = (float)(*di++);	
  
 
  float Average_IN_o = 0.;
  for (int ind=0; ind<w*h; ind++) Average_IN_o += denoised_image[ind];
  Average_IN_o /= (w*h);
  cout << "Average_intensity = "<< Average_IN_o << endl;
  
  for (int ind=0; ind<w*h; ind++) IN_o[ind] += (Average_IN_i-Average_IN_o);
   
  Average_IN_o = 0.;
  for (int ind=0; ind<w*h; ind++) Average_IN_o += IN_o[ind];
  Average_IN_o /= (w*h);
 
  cout << "Average_intensity = "<< Average_IN_o << endl;
  cout <<" "<< endl;
}

/*=============================================================================*/




 
 
 
/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : SparseHessianVariationDenoising 
_Subject  :

-----------------------------------------------------------------------------*/

void SparseHessianVariationDenoising(float *IN_i, float *IN_o, int w, int h, const double regularization_parameter,
										const double weighting_parameter, const int nb_iterations, float Max_i)  

{
 	printf("\nRegularization parameter (default: 100) : %2.2f\n", regularization_parameter);
  	printf("Sparsity parameter (default: 0.6 in [0.1 - 0.9]: %2.2f \n", weighting_parameter);
  	printf("Maximum number of iterations: %d\n", nb_iterations);
  
  	bool verbose = false;
  	double normalized_L2_error_threshold = 1e-6;
  
  	float Average_IN_i= 0.;
	for (int ind=0; ind<w*h; ind++) Average_IN_i += IN_i[ind];
	Average_IN_i /= (w*h);
	//cout << "Average_intensity = "<< Average_IN_i << endl;
	
  	CImg<double> noisy_image(w,h);
  	double *ni = noisy_image.data(0,0,0,0);
  	for (int ind=0; ind<w*h; ind++) *ni++ = (double)(*IN_i++);
	  
 	int img_width = noisy_image.width();
  	int img_height = noisy_image.height();
  	double sqrt2 = sqrt(2.);

  	// Splitting parameters
  	double dual_step = MAX(0.001, MIN(0.01, regularization_parameter));
  	double primal_step = 0.99 / (0.5 + (64 * pow(weighting_parameter, 2.)
  							+ pow(1 - weighting_parameter, 2.)) * dual_step);
  	
	cout << "Eta = " << primal_step << "; Nu = " << dual_step << endl;
  	double primal_weight = primal_step * weighting_parameter;
  	double primal_weight_comp = primal_step * (1 - weighting_parameter);
  	double dual_weight = dual_step * weighting_parameter;
  	double dual_weight_comp = dual_step * (1 - weighting_parameter);

	// Initializations
  	CImg<double> denoised_image(noisy_image);
  	CImgList<double> dual_images(4, img_width, img_height, 1, 1, 0.);
  	CImg<double> auxiliary_image;

  	// Deconvolution process
  	double img_L2_norm = sqrt(noisy_image.get_sqr().sum());
  	double tmp, dxx, dyy, dxy, min_val=0., max_val=(double)(Max_i), dxx_adj, dyy_adj, dxy_adj;
  	
	for (int iter = 0; iter < nb_iterations; iter++) {
  		// Primal optimization
  		auxiliary_image = denoised_image;

  		cimg_forXY(denoised_image, x, y){
			tmp = denoised_image(x, y) - primal_step * (denoised_image(x, y) - noisy_image(x, y));
  			//min_val = lower_bound(x, y);
  			//max_val = upper_bound(x, y);

  			dxx_adj = dyy_adj = dxy_adj = 0.;

  			if ((x > 0) && (x < img_width - 1))
  				dxx_adj = dual_images[0](x - 1, y) - 2 * dual_images[0](x, y) + dual_images[0](x + 1, y);

  			if ((y > 0) && (y < img_height - 1))
  				dyy_adj = dual_images[1](x, y - 1) - 2 * dual_images[1](x, y) + dual_images[1](x, y + 1);

  			if ((x == 0) && (y == 0))
  				dxy_adj = dual_images[2](x, y);
  			if ((x > 0) && (y == 0))
  				dxy_adj = dual_images[2](x, y) - dual_images[2](x - 1, y);
  			if ((x == 0) && (y > 0))
  				dxy_adj = dual_images[2](x, y) - dual_images[2](x, y - 1);
  			if ((x > 0) && (y > 0))
  				dxy_adj = dual_images[2](x, y) - dual_images[2](x - 1, y) - dual_images[2](x, y - 1) + dual_images[2](x - 1, y - 1);

  			tmp -= (primal_weight * (dxx_adj + dyy_adj + sqrt2 * dxy_adj) + primal_weight_comp * dual_images[3](x, y));
  			denoised_image(x, y) = MAX(min_val, MIN(max_val, tmp));
  		}	

  		// Stopping criterion
  		double error = sqrt((denoised_image - auxiliary_image).get_sqr().sum()) / img_L2_norm;
  		if (verbose)
  			if (iter % MAX(1, nb_iterations / 10) == 0)
  				cout << "Iteration #" << iter << " ; Error = " << error
  				<< "; Threshold = " << normalized_L2_error_threshold
  				<< endl; //debug
		if (iter>1 && error < normalized_L2_error_threshold){
      	  cout <<"Number of actual iterations: " <<  iter << endl;
      	  break;
		}

  		// Dual optimization
  		cimg_forXY(auxiliary_image, x, y)
  			auxiliary_image(x, y) = 2 * denoised_image(x, y) - auxiliary_image(x, y);
  		
		cimg_forXY(auxiliary_image, x, y){
  			if ((x > 0) && (x < img_width - 1)) {
  				dxx = auxiliary_image(x + 1, y) - 2 * auxiliary_image(x, y) + auxiliary_image(x - 1, y);
  				dual_images[0](x, y) += dual_weight * dxx;
  			}
  			if ((y > 0) && (y < img_height - 1)) {
  				dyy = auxiliary_image(x, y + 1) - 2 * auxiliary_image(x, y) + auxiliary_image(x, y - 1);
  				dual_images[1](x, y) += dual_weight * dyy;
  			}

  			if ((x < img_width - 1) && (y < img_height - 1)) {
  				dxy = auxiliary_image(x + 1, y + 1) - auxiliary_image(x + 1, y)
  						- auxiliary_image(x, y + 1) + auxiliary_image(x, y);
  				dual_images[2](x, y) += sqrt2 * dual_weight * dxy;
  			}

  			dual_images[3](x, y) += dual_weight_comp * auxiliary_image(x, y);
  		}

  		cimg_forXY(auxiliary_image, x, y){
  			double tmp = MAX(1.,
  					1. / regularization_parameter
  							* sqrt(
  									pow(dual_images[0](x, y), 2.)
  											+ pow(dual_images[1](x, y), 2.)
  											+ pow(dual_images[2](x, y), 2.)
  											+ pow(dual_images[3](x, y), 2.)));
  			dual_images[0](x, y) /= tmp;
  			dual_images[1](x, y) /= tmp;
  			dual_images[2](x, y) /= tmp;
  			dual_images[3](x, y) /= tmp;
  		}
  	} // endfor (int iter = 0; iter < nb_iters_max; iter++)
	  
	  
  //return denoised_image;
  double *di = denoised_image.data(0,0,0,0);
  for (int ind=0; ind<w*h; ind++) IN_o[ind] = (float)(*di++);
 
  float Average_IN_o = 0.;
  for (int ind=0; ind<w*h; ind++) Average_IN_o += denoised_image[ind];
  Average_IN_o /= (w*h);
  //cout << "Average_intensity = "<< Average_IN_o << endl;
  
  for (int ind=0; ind<w*h; ind++) IN_o[ind] += (Average_IN_i-Average_IN_o);
   
  Average_IN_o = 0.;
  for (int ind=0; ind<w*h; ind++) Average_IN_o += IN_o[ind];
  Average_IN_o /= (w*h);
 
  //cout << "Average_intensity = "<< Average_IN_o << endl;	
  cout <<" "<< endl; 
}

/*=============================================================================*/
