#include <stdio.h>
#include <math.h>
#include "DCT2Dnxn.h"
#include <assert.h>

// 2D DCT of a nxn patches. The result is restored in-place.
void DCT2Dnxn(vector< vector< float > >& patch, int n, int flag)
{
  std::vector< vector< float > > M;
  std::vector< vector< float > > T;
  std::vector< vector< float > > tT;
  std::vector< vector< float > > DCT;

  T.resize(n);
  tT.resize(n);
  M.resize(n);
  DCT.resize(n);
  
  for (int i=0; i<n; i++){
    T[i].resize(n);
    tT[i].resize(n);
    M[i].resize(n); 
    DCT[i].resize(n); 
  }

  // 1D DCT matrix
  for (int j=0; j<n; j++) T[0][j] = 1./sqrt(n);  
  for (int i=1; i<n; i++) for (int j=0; j<n; j++) T[i][j] = sqrt(2./n)*cos(((2.*j+1)*i*3.14159265358979323846)/(2.*n));
  
  // Transpose matrix
  for (int i=0; i<n; i++)  for (int j=0; j<n; j++) tT[i][j] = T[j][i];
    
  // Initialization
  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++){
      M[i][j] = 0.; 
      DCT[i][j] = 0.;
    } 

  if (flag==1){ // forward DCT
    for (int i=0; i<n; i++) for (int j=0; j<n; j++) for (int k=0; k<n; k++) M[i][j] += T[i][k]*patch[k][j];   
    for (int i=0; i<n; i++) for (int j=0; j<n; j++) for (int k=0; k<n; k++) DCT[i][j] += M[i][k]*tT[k][j]; 
  }
  else{ // backward DCT
    for (int i=0; i<n; i++) for (int j=0; j<n; j++) for (int k=0; k<n; k++) M[i][j] += tT[i][k]*patch[k][j];
    for (int i=0; i<n; i++) for (int j=0; j<n; j++) for (int k=0; k<n; k++)  DCT[i][j] += M[i][k]*T[k][j];
  }

  // Save
  for (int j=0; j<n; j++) for (int i=0; i<n; i++) patch[j][i] = DCT[i][j];
}
