/*=============================================================================

_Header

_Module_Name 	:  NLmeans.h

_Directory	: 

_Description 	: Declaration de  NLmeans.cpp

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 12/06/14

_end

=============================================================================*/





/*===========================================================================*/

#ifndef _NLmeans_H_
#define _NLmeans_H_

/*===========================================================================*/

#include "define.h"
#include "include.h"
#include "CImg.h"
 
using namespace cimg_library;
// The undef below is necessary when using a non-standard compiler.
#ifdef cimg_use_visualcpp6
#define std  
#endif

/*===========================================================================*/

extern void  NLmeans(float *_pixmap, float *I_out, int w, int h, float SigmaManuel, 
						int TailleMotifs, int TailleVois,  double lambda);
						
extern void  BayesNLmeans(float *_pixmap, float *Iout, int w, int h, float SigmaManuel, 
							int TailleMotifs, int TailleVois, int Iter);

/*===========================================================================*/

#endif

/*===========================================================================*/

