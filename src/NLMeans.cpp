/*=============================================================================

_Header

_Module_Name 	: NLmeans.cpp

_Directory	: 

_Description 	: Description de NLmeans.cpp

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 12/06/14

_end

=============================================================================*/




 

/*===========================================================================*/

#include "include.h"
#include "define.h"
#include "IOmodules/ByteImage3D.h"
#include "MAD.h"
#include "IOmodules/Sauvegarde.h"
#include "NLMeans.h"
#include "CImg.h"

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : NLmeans 
_Subject  :

-----------------------------------------------------------------------------*/

void  NLmeans(float *_pixmap, float *I_out, int w, int h, float SigmaManuel, 
				int TailleMotifs, int TailleVois, double lambda)
{
  int   i_, j_, i, j, k, l;
  int   ind, ind_w;
  float f;
  float sum_weight;
  float weight;
  float sig, sig2;
  float n;




  /* Allocation dynamique */
  /* ==================== */
  float *residuG  = new float[w*h];
  float *f_star   = new float[w*h];





  /* Etapes d'initialisation de l'agorithme */
  /* ====================================== */

  // Estimation de la variance du bruit 
  // ---------------------------------- 
  PseudoResidu(_pixmap,residuG,w,h);
  if (SigmaManuel==0.) sig = SigmaLTS(residuG,w*h);
  else                 sig = SigmaManuel; 
  //printf("   ecart-type du bruit blanc gaussien  : %2.2f\n", sig);
  sig2 = sig*sig;
 
  // Tailles de patch et de voisinage 
  // -------------------------------- 
  int du       = TailleMotifs;
  int dv       = TailleMotifs;
  int delta    = TailleVois;
  float Np     = (2*du+1)*(2*dv+1); 

  printf("Patch size (default: 7 x 7): %d x %d\n", 2*TailleMotifs+1,  2*TailleMotifs+1);
  printf("Neighborhood size (default: 15 x 15): %d x %d\n", 2*delta+1,  2*delta+1);





  /* Procedure NL Means */
  /* ================== */
  //printf("\n------------------\n");
  //printf("Procedure NL Means\n");
  //printf("------------------\n");

  double wmax = 0.;
  printf("Smoothing parameter lambda (default: 3.5): %2.2f\n", lambda);
  //lambda /= sqrt(Np);
  float h2   = 2*lambda*lambda*sig2;
  printf("h2 = 2 (lambda x sigma)^2 : %2.2f\n\n", h2);
  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      ind=i+w*j;
      sum_weight = 0;
      f          = 0.;
      n          = 0.;
      wmax       = 1.e-3;
 
      for (k=-delta; k<=delta; k++)
	for (l=-delta; l<=delta; l++){
	  i_ = i+k;
	  j_ = j+l;

	  float cout = 0.;

	  if (INCLUS(i_,j_,w,h)!=0){
	    ind_w=i_+w*j_;

	    for (int u=-du; u<=du; u++)
	      for (int v=-dv; v<=dv; v++){
		int p  = i+u;
		int q  = j+v;
		int p_ = i_+u;
		int q_ = j_+v;

		if ((INCLUS(p,q,w,h)!=0) && (INCLUS(p_,q_,w,h)!=0)){
		  int pq = p+w*q;
		  int p_q_ = p_+w*q_;
		  cout += (_pixmap[pq]-_pixmap[p_q_])*(_pixmap[pq]-_pixmap[p_q_]);
		}
	      }
		    
	    weight = exp(-cout/h2);
	  
	    if ((k!=0) || (l!=0)) wmax = MAX(wmax,weight); //wmax = MAX(0.00001,MAX(wmax,weight));

	    f          += weight*_pixmap[ind_w];
	    sum_weight += weight;
	    n++; 
	  }
	} 
      
      f          += (wmax-1)*_pixmap[ind];
      sum_weight += wmax-1;

      f_star[ind]   = f/sum_weight;
    }

  for (ind=0; ind< w*h; ind++) I_out[ind] = f_star[ind];
}

/*=============================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : BayesNLmeans
_Subject  :

-----------------------------------------------------------------------------*/

void  BayesNLmeans(float *_pixmap, float *Iout, int w, int h, float SigmaManuel, 
					int TailleMotifs, int TailleVois, int Iter)
{
  int   i_, j_, i, j, k, l;
  int   ind, ind_w;
  int   dimP = (2*TailleMotifs+1);
  float lambda = 1.;
  float weight;
  float sig, sig2;
  float n;

  float ICmean = 3.0;
  float ICvar = 1.65;
  int SpatialSampling = 1;



  /* ALLOCATION DYNAMIQUE */
  /* ==================== */
  unsigned char *I_ima  = new unsigned char[w*h];
  float *Iwmax          = new float[w*h];
  float *residuG        = new float[w*h];
  float *f_star         = new float[w*h];
  float *w_star         = new float[w*h];
  float *f_temp         = new float[w*h];
  float *f              = new float[dimP*dimP];
  float *var            = new float[dimP*dimP];
  float *sum_weight     = new float[dimP*dimP];




  /* ETAPES D'INITIALISATION DE L'ALGORITHME */
  /* ======================================= */
  PseudoResidu(_pixmap,residuG,w,h);
  if (SigmaManuel==0.) sig = SigmaLTS(residuG,w*h);
  else                 sig = SigmaManuel;
  sig2 = sig*sig;
  float *sig_I = new float[w*h];
  for(i=0; i<w*h; i++) sig_I[i] = sig2;
  
  int du           = TailleMotifs;
  int dv           = TailleMotifs;
  int delta        = TailleVois;
  float Np         = (2*du+1)*(2*dv+1); 
  float h2         = 2*lambda*lambda;
  float Msqrt2cout = sqrt(2*Np-1);
  float M2cout     = 2*Np-1;
  float exp_M2cout = exp(-M2cout/2);
  float wmax       = 0.;
  float w0         = 0.;
  float WMAX       =-1.;
  
  printf("Patch size (default: 7 x 7): %d x %d\n", 2*TailleMotifs+1,  2*TailleMotifs+1);
  printf("Neighborhood size (default: 15 x 15): %d x %d\n", 2*delta+1,  2*delta+1);
 
  for(i=0; i<w*h; i++){
    f_temp[i] = _pixmap[i];
    f_star[i] = _pixmap[i];
  }


  /* Estimation des moyennes et variances locales sur les blocs */
  /* ---------------------------------------------------------- */
  float SupV   = ICvar; 
  float *npix  = new float[w*h];
  float *SupM  = new float[w*h];
  float *Imean = new float[w*h];
  float *Ivar  = new float[w*h];  

  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      ind=i+w*j;
      for (int u=-du; u<=du; u++)
	for (int v=-dv; v<=dv; v++){
	  if (INCLUS(i+u,j+v,w,h)!=0){
	    Imean[ind] += _pixmap[i+u+w*(j+v)];
	    npix[ind]++;
	  }
	}
      Imean[ind] /= npix[ind]; 
      SupM[ind]   = (ICmean*sig)/sqrt(npix[ind]);
    }
 
  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      ind=i+w*j;
      for (int u=-du; u<=du; u++)
	for (int v=-dv; v<=dv; v++){
	  if (INCLUS(i+u,j+v,w,h)!=0){
	    Ivar[ind] += (_pixmap[i+u+w*(j+v)]-Imean[ind])*(_pixmap[i+u+w*(j+v)]-Imean[ind]);
	  }
	}
      Ivar[ind] /= npix[ind];   
      Ivar[ind] = MAX(Ivar[ind], 1e-5);
    }


  /* Taille des dictionaires */
  /* ----------------------- */
  int NmaxDic =(2*delta+1)*(2*delta+1);
  int *HDic = new int[NmaxDic+1];
  for (i=0; i<NmaxDic; i++) HDic[i]=0;
    
  int sum_T=0;
  for (i=0; i<(int)(0.5*NmaxDic); i++)
    sum_T +=  HDic[i];
  
 
  
  

  /* PROCEDURE DE DEBRUITAGE */
  /* ======================= */
  for (int iter=1; iter<=Iter; iter++){

    for(i=0; i<w*h; i++){
      f_temp[i] = f_star[i];
      f_star[i] = 0;
      w_star[i] = 0.;
      //Var_star[i] = 0.;
      Iwmax[i]  = _pixmap[i];
    }
    if (iter==2)
      h2 = 0.5*lambda*lambda;

	printf("Iteration #%d - ", iter);
    printf("Smoothing parameter: %2.2f\n", sqrt(h2));	


    /* Estimation par blocs */
    /* -------------------- */     
    for(i=0; i<w; i+=SpatialSampling)
      for(j=0; j<h; j+=SpatialSampling){
	ind  = i+w*j;
	
	n    = 0.;
	wmax = 0.;
	
	for (int dim=0; dim<dimP*dimP; dim++){
	  f[dim]          = 0;
	  var[dim]        = 0;
	  sum_weight[dim] = 0;
	}
	
	for (k=-delta; k<=delta; k++)
	  for (l=-delta; l<=delta; l++){
	    i_ = i+k;
	    j_ = j+l;
	    
	    float cout = 0.; 
	    
	    if (INCLUS(i_,j_,w,h)!=0){
	      ind_w=i_+w*j_;

	      if ((fabs(Imean[ind_w]-Imean[ind])<SupM[ind]) && 
		  ((MAX(Ivar[ind_w],Ivar[ind])/MIN(Ivar[ind_w],Ivar[ind]))<SupV)){
		  
		  	Np=0;
		  
		  	for (int u=-du; u<=du; u++) 
		    	for (int v=-dv; v<=dv; v++){
		      	int p  = i+u;
		      	int q  = j+v;
		      	int p_ = i_+u;
		      	int q_ = j_+v;
		      
		    if ((INCLUS(p,q,w,h)!=0) && (INCLUS(p_,q_,w,h)!=0)){
				int pq = p+w*q;
				int p_q_ = p_+w*q_;
				cout += (_pixmap[pq]-f_temp[p_q_])*(_pixmap[pq]-f_temp[p_q_]);
				Np++;
		    	}
		    }
		  
		  Msqrt2cout = sqrt(2*Np-1);
		  M2cout     = 2*Np-1;
		  
		  float dist = 2*cout/(h2*sig_I[ind]);
		  weight = exp(-(dist + M2cout - 2*Msqrt2cout*sqrt(dist))/2);
		}
	      else weight = 0.;
	      
	

	      if ((k!=0) || (l!=0)) wmax = MAX(wmax,weight);
	      if ((k==0) && (l==0)) w0   = weight;
	      
		  
		  for (int u=-du; u<=du; u++)
			for (int v=-dv; v<=dv; v++){
		  	  int p_ = i_+u;
		  	  int q_ = j_+v;
		  	  int p_q_ = p_+w*q_;

		      if (INCLUS(p_,q_,w,h)!=0){
		    	  f[(u+du)*dimP+v+dv]          += weight*f_temp[p_q_];
		    	  var[(u+du)*dimP+v+dv]        += SQR(weight)*sig_I[ind];
		    	  sum_weight[(u+du)*dimP+v+dv] += weight;
		  	} 
		}	      
	      n++;
	    }    
	  } 
	
	WMAX = MAX(WMAX,wmax);
	wmax = MAX(WSUP,wmax);
	
	if (wmax<w0)
	  Iwmax[ind] = wmax; 
	
	wmax = MAX(w0,wmax);

	for (int u=-du; u<=du; u++)
	  for (int v=-dv; v<=dv; v++){
	    int p  = i+u;
	    int q  = j+v;
	    int pq = p+w*q;
	    
	    if (INCLUS(p,q,w,h)!=0){ 
	      sum_weight[(u+du)*dimP+v+dv] += wmax-w0;
	      f[(u+du)*dimP+v+dv]          += (wmax-w0)*f_temp[pq];
	      var[(u+du)*dimP+v+dv]        += (SQR(wmax)-SQR(w0))*sig_I[ind];	     	    

	      f_star[pq]   += f[(u+du)*dimP+v+dv]/sum_weight[(u+du)*dimP+v+dv];	      
	      w_star[pq]   += 1;	      
	    }
	  }
      }
    
    for(i=0; i<w; i++)
      for(j=0; j<h; j++){
		  ind=i+w*j;
		  f_star[ind]   /= (w_star[ind]);
      } 
  }




  
  /* SAUVEGARDE DES RESULTATS */
  /* ======================== */ 
  for (ind=0; ind< w*h; ind++) Iout[ind] = f_star[ind];
   
  /*
  delete [] I_ima;
  delete [] Iwmax;
  delete [] residuG;
  delete [] f_star;
  delete [] w_star;
  delete [] f_temp;
  delete [] f;
  delete [] var;
  delete [] sum_weight;
  delete [] sig_I;
  delete [] npix;
  delete [] SupM;
  delete [] Imean;
  delete [] Ivar;
  */
}

/*=============================================================================*/
