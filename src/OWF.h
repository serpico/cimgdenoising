/*=============================================================================

_Header

_Module_Name 	:  OWF.h

_Directory	: 

_Description 	: Declaration de  OWF.cpp

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 05/02/16

_end

=============================================================================*/





/*===========================================================================*/

#ifndef _OWF_H_
#define _OWF_H_

/*===========================================================================*/

#include "define.h"
#include "include.h"
#include "CImg.h"
 
using namespace cimg_library;
// The undef below is necessary when using a non-standard compiler.
#ifdef cimg_use_visualcpp6
#define std  
#endif

/*===========================================================================*/

extern void QuickSort(double *a,int left,int right);
extern double WeightsWidth(double *a, double v, int wd);
extern void OWF(float *noisy, float *oracle, float *denoisy, float v,  int nw, 
		int np, int nx, int ny);
extern void OWF_oracle(float *noisy, float *oracle, float *denoisy, float v,  
		       int nw, int nx, int ny);
extern void OWF2images(float *I1, float *I2, float *denoisy, float v,  int nw, 
		       int np, int nx, int ny);

/*===========================================================================*/

#endif

/*===========================================================================*/

