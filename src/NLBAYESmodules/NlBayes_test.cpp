/*
 * Copyright (c) 2013, Marc Lebrun <marc.lebrun.ik@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file NlBayes.cpp
 * @brief NL-Bayes denoising functions
 *
 * @author Marc Lebrun <marc.lebrun.ik@gmail.com>
 **/

#include <stdio.h>

#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <math.h>

#include "NlBayes.h"
#include "LibMatrix.h"
#include "LibImages.h"
#include "Utilities.h"

#ifdef _OPENMP
#include <omp.h>
#endif

using namespace std;


void  runNlBayes(
	      float *IN
,             float *IO
,             int w
,             int h
,	int p_useArea1
,	int p_useArea2
,	float p_sigma
,       int p_verbose
){

}
