/*=============================================================================
	   copyright(c) 1999-2001 by INRA-Biometrie Jouy-en-Josas.
      	                  All Rights Reserved.

-------------------------------------------------------------------------------

_Header

_Module_Name 	: ByteImage3D.C

_Directory	: 

_Description 	: Definition de la classe "ByteImage"

_Authors	: Mark HOEBEKE & Charles KERVRANN

_Contents       : ByteImage::get()
                  ByteImage::put()
		  ByteImage::get_pixmap()
		  ByteImage::set_pixmap()
		  ByteImage::save()
		  ByteImage::save_pixmap()
		  ByteImage::min()
		  ByteImage::max()

_Last Release   : 24/04/01

_end

=============================================================================*/

#include "include.h"
#include "define.h"
#include "ByteImage3D.h"

/*===========================================================================*/






/*===========================================================================*/

ByteImage::ByteImage(int w, int h, int d, unsigned char v) {
  _width=w;
  _height=h;
  _depth=d;
  _pixmap=new unsigned char[w*h*d];
  int dummy=v;
  memset(_pixmap,dummy,w*h*d);
}

ByteImage::ByteImage(int w, int h, int d, unsigned char *pixmap) {
  _width=w;
  _height=h;
  _depth=d;
  _pixmap=pixmap;
}

ByteImage::ByteImage(int w, int h, int d, char *s)
{
  FILE *f;
  unsigned char x;
  char header[100];

  _width=w;
  _height=h;
  _depth=d;
  _pixmap=new unsigned char[w*h*d];

  f=fopen(s,"r+");

  if (_depth==1){
    fseek(f, 0, 2);
    int filesize = ftell(f);
    int headersize = filesize - w*h*d;
    fseek(f,0,0);
    fread(header,headersize,1,f);
  }

  for (int index=0;index<_width*_height*_depth;index++){
    fread(&x,sizeof(unsigned char),1,f);
    _pixmap[index]=x;
  }

  fclose(f);
}

/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : int ByteImage::get(const int x, const int y, const z)
_Subject  : Get an intensity value at site (x,y,z)

-----------------------------------------------------------------------------*/

int ByteImage::get(const int x, const int y, const int z) {
  return (int)_pixmap[z*_width*_height+y*_width+x];
}

/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : void ByteImage::put(const int x, const int y, const z, const int pixel)
_Subject  :  Put an intensity value at site (x,y,z)

-----------------------------------------------------------------------------*/

void ByteImage::put(const int x, const int y, const int z, const int pixel)
{
  _pixmap[z*_width*_height+y*_width+x]=(unsigned char)pixel;
}

/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : unsigned char *ByteImage::get_pixmap(void)
_Subject  :  Get an 8-bits image

-----------------------------------------------------------------------------*/

unsigned char *ByteImage::get_pixmap(void) {
  return _pixmap;
}

/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : void ByteImage::set_pixmap(unsigned char *pixmap)
_Subject  :  Set an 8-bits image

-----------------------------------------------------------------------------*/

void ByteImage::set_pixmap(unsigned char *pixmap) {
  delete[] _pixmap;

  _pixmap=pixmap;
}

/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : void ByteImage::save(char *fname)
_Subject  :  Save the  8-bits image

-----------------------------------------------------------------------------*/

void ByteImage::save(char *fname) {
  FILE *f;
  unsigned char x;

  f=fopen(fname,"w+");
  if (_depth==1){
    fprintf(f,"P5\n");
    fprintf(f,"%d %d\n", _width, _height);
    fprintf(f,"255\n");
}
  for (int index=0;index<_width*_height*_depth;index++){
    x=_pixmap[index];
    fwrite(&x, sizeof(unsigned char), 1, f);
  }
  fclose(f);
}

/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : int ByteImage::min(const int method)
_Subject  :  Minimum intensity value of an image

-----------------------------------------------------------------------------*/

int ByteImage::min(void) {
  int min=_pixmap[0];
  for (int index=0;index<_width*_height*_depth;index++)
    min=_pixmap[index]<min?_pixmap[index]:min;
  return min;
}
/*===========================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  :  int ByteImage::max(void)
_Subject  : Maximum intensity value of an image

-----------------------------------------------------------------------------*/

int ByteImage::max(void) {
  int max=_pixmap[0];
  for (int index=0;index<_width*_height*_depth;index++)
    max=_pixmap[index]>max?_pixmap[index]:max;
  return max;
}

/*===========================================================================*/





/*===========================================================================*/

ByteImage::ByteImage(const ByteImage &bi) {
  delete[] _pixmap;
  _width=bi._width;
  _height=bi._height;
  _depth=bi._depth;
  _pixmap=new unsigned char[_width*_height*_depth];
  memcpy(_pixmap,bi._pixmap,_width*_height*_depth);
}

ByteImage & ByteImage::operator=(const ByteImage &bi) {
  if (this !=&bi) {
    delete[] _pixmap;
    _width=bi._width;
    _height=bi._height;
    _depth=bi._depth;
    _pixmap=new unsigned char[_width*_height*_depth];
    memcpy(_pixmap,bi._pixmap,_width*_height*_depth);
  }
  return *this;
}

ByteImage::~ByteImage() {
  delete[] _pixmap;
}

/*===========================================================================*/
