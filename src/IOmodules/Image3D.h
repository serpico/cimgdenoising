/*=============================================================================
	
_Header

_Module_Name 	: Image3D.h

_Directory	: 

_Description 	: Declaration de la classe "Image"

_Authors	: Mark HOEBEKE & Charles KERVRANN

_Contents       :

_Last Release   : 24/04/01

_end

=============================================================================*/






/*===========================================================================*/

#ifndef _IMAGE_H_
#define _IMAGE_H_

/*===========================================================================*/

class Image {

public:
  enum { INCLUDE_0=0, EXCLUDE_0=1};
  virtual int get(const int, const int, const int)=0;
  virtual void put(const int, const int, const int, const int)=0;
  inline int width(void) { return _width;};
  inline int height(void) { return _height;};
  inline int depth(void) { return _depth;};
  virtual void save(char *)=0;
  virtual int min()=0;
  virtual int max()=0;
protected:
  int _width;
  int _height;
  int _depth;
};

#endif

/*===========================================================================*/
