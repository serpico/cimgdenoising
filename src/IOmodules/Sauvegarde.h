/*=============================================================================

_Header

_Module_Name 	: Sauvegarde.h

_Directory	: 

_Description 	: Declaration de Sauvegarde.cc

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 17/11/03

_end

=============================================================================*/





/*===========================================================================*/

#ifndef _SAUVEGARDE_H_
#define _SAUVEGARDE_H_

/*===========================================================================*/

#include "define.h"
#include "include.h"

/*===========================================================================*/

extern void  sauvegarde(float *matF, int w, int h, char *name);
extern void  sauvegardeINR(float *matF, int w, int h, char *name);
extern void  sauvegardeF(float *matF, int w, int h, char *name);
extern void  sauvegardeUC(unsigned char *matUC, int w, int h, char *name);
extern float comp_psnr(float *I0, int w, int h, float *I); 
extern float comp_psnr_0(float *I0, int w, int h, float *I);
extern float comp_mse(float *I0, int w, int h, float *I); 
extern float comp_score(float *Y, float *X, float *Cov, float sigma, int w, int h);
extern float comp_sure(float *Y, float *X, float div, float sigma, int w, int h);
extern float comp_psnr_clip255(float *I0, int w, int h, float *I);
extern float comp_psnr_clip255_vois4(float *I0, int w, int h, float *I);
extern float comp_psnr_clip255_vois4_mean(float *I0, int w, int h, float *I);
extern void  wiener(float *I0, int w, int h, float sig2, float *I_std, float *I);

/*===========================================================================*/

#endif

/*===========================================================================*/

