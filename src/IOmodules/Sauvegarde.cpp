/*=============================================================================

_Header 

_Module_Name 	: Sauvegarde.cpp

_Directory	:

_Description 	: Description de Sauvegarde.cpp

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 17/05/07

_end

=============================================================================*/






/*============================================================================*/

#include "include.h"
#include "define.h"
#include "ByteImage3D.h"


/*============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : sauvegarde
_Subject  :

-----------------------------------------------------------------------------*/

void sauvegarde(float *matF, int w, int h, char *name)
{
  int i;
  unsigned char *Ima = new unsigned char[w*h];

  for (i=0; i<w*h; i++)
    Ima[i] = (unsigned char)(MAX(0,MIN(255,(matF[i]+0.5))));

  Image * I_SAVE=new ByteImage(w,h,1,Ima);
  I_SAVE->save(name);

  delete[] Ima;

}

/*============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : sauvegardeINR
_Subject  :

-----------------------------------------------------------------------------*/

void sauvegardeINR(float *matF, int w, int h, char *name)
{

  FILE  *file;    
  int i;

  file=fopen(name, "w");
  fprintf(file, "#INRIMAGE-4#{\n");
  fprintf(file, "XDIM=%d\n", w);
  fprintf(file, "YDIM=%d\n", h);
  fprintf(file, "ZDIM=1\n");
  fprintf(file, "TYPE=float\n");
  fprintf(file, "PIXSIZE=32 bits\n");
  fprintf(file, "CPU=decm\n");
  fprintf(file, "##}\n");

  for (i=0; i<w*h; i++){
    float val = matF[i];
    fwrite(&val,sizeof(float),1,file);
  }
  
  fclose(file);

}

/*============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : sauvegardeF
_Subject  :

-----------------------------------------------------------------------------*/

void sauvegardeF(float *matF, int w, int h, char *name)
{
  int i;
  float M = matF[0];
  unsigned char *Ima = new unsigned char[w*h];

  for (i=0; i<w*h; i++)
    if (matF[i]>M)
      M = matF[i];

  if (M>1)
    for (i=0; i<w*h; i++)
      Ima[i] = (unsigned char)(255*(matF[i]/M));
  else
    for (i=0; i<w*h; i++)
      Ima[i] = (unsigned char)(255*matF[i]);

  Image * I_SAVE=new ByteImage(w,h,1,Ima);
  I_SAVE->save(name);

  delete[] Ima;

}

/*============================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : sauvegardeUC
_Subject  :

-----------------------------------------------------------------------------*/

void sauvegardeUC(unsigned char *matUC, int w, int h, char *name)
{
  int i;
  float M = matUC[0];
  unsigned char *Ima = new unsigned char[w*h];

  for (i=0; i<w*h; i++)
    if (matUC[i]>M)
      M = (float)(matUC[i]);

  for (i=0; i<w*h; i++)
    Ima[i] = (unsigned char)(255*((float)(matUC[i])/M));

  Image * I_SAVE=new ByteImage(w,h,1,Ima);
  I_SAVE->save(name);

  delete[] Ima;

}

/*============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : COMP_PSNR
_Subject  :

-----------------------------------------------------------------------------*/

float comp_psnr(float *I0, int w, int h, float *I)
{
  int i;
  float mse=0.;
  float psnr;

  for (int i=0; i<w*h; i++) mse += (I[i] - I0[i])*(I[i] - I0[i]);
  mse /= (w*h);
  psnr = 10*log10(255*255/mse);

  printf("PSNR = %2.4f db  / RMSE = %2.4f \n", psnr, sqrt(mse));
 
  return(psnr);
}

/*============================================================================*/

 




/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : COMP_PSNR_0
_Subject  :

-----------------------------------------------------------------------------*/

float comp_psnr_0(float *I0, int w, int h, float *I)
{
  int i;
  float mse=0.;
  float psnr;
  int Np=0;

  for (int i=0; i<w*h; i++){
    if (I[i]!=0.){
      mse += (I[i] - I0[i])*(I[i] - I0[i]);
      Np++;
    }
  }

  mse /= Np;
  psnr = 10*log10(255*255/mse);

  printf("Rate =%f\n", (float)(Np)/(w*h));
  printf("PSNR_0 = 10 log10(255^2/MSE) = %f db  / RMSE = %2.4f \n\n", psnr, sqrt(mse));

  return(psnr);
}

/*============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : COMP_MSE
_Subject  :

-----------------------------------------------------------------------------*/

float comp_mse(float *I0, int w, int h, float *I)
{
  int i;
  float mse=0.;

  for (int i=0; i<w*h; i++) mse += (I[i] - I0[i])*(I[i] - I0[i]);
  mse /= (w*h);

  printf("   RMSE = %2.4f \n", sqrt(mse));

  return(mse);
}

/*============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : COMP_SCORE
_Subject  :

-----------------------------------------------------------------------------*/

float comp_score(float *Y, float *X, float *Cov, float sigma, int w, int h)
{
  int i;
  float s2 = sigma*sigma;
  float score = 0.;

  for (int i=0; i<w*h; i++) score += (Y[i] - X[i])*(Y[i] - X[i]) + 2*s2*Cov[i];
  score -= (w*h)*s2;

  printf("SCORE = %2.4f sigma=%2.4f \n", score/(w*h), sigma);

  return(score);
}

/*============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : COMP_SURE
_Subject  :

-----------------------------------------------------------------------------*/

float comp_sure(float *Y, float *X, float div, float sigma, int w, int h)
{
  int i;
  float s2 = sigma*sigma;
  float sure = 0.;

  for (int i=0; i<w*h; i++) sure += (Y[i] - X[i])*(Y[i] - X[i]);
  sure += 2*s2*div - (w*h)*s2;

  printf("Monte-Carlo SURE = %2.4f sigma=%2.4f \n", sure/(w*h), sigma);

  return(sure);
}

/*============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : COMP_PSNR_CLIP255
_Subject  :

-----------------------------------------------------------------------------*/

float comp_psnr_clip255(float *I0, int w, int h, float *I)
{
  int i;
  float mse=0.;
  float psnr;

  for (int i=0; i<w*h; i++){
      float I_ = MIN(255.,MAX(0.,I[i]));
      mse += (I_ - I0[i])*(I_ - I0[i]);
  }
  mse /= (w*h);
  psnr = 10*log10(255*255/mse);

  printf("PSNR = 10 log10(255^2/MSE) = %f db  / RMSE = %2.4f \n", psnr, sqrt(mse));

  return(psnr);
}

/*============================================================================*/







/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : COMP_PSNR_CLIP255_VOIS4
_Subject  :

-----------------------------------------------------------------------------*/

float comp_psnr_clip255_vois4(float *I0, int w, int h, float *I)
{
  float mse=0.;
  float psnr;

  for(int i=0; i<w; i++)
    for(int j=0; j<h; j++){
      float I_E, I_W, I_N, I_S;
      int ind  = i+w*j;
      float I_ = I[ind];

      if (I[ind]<0.){
	if (i-1>=0) {if (I[(i-1)+w*j]>0.) I_W= I[(i-1)+w*j]; else I_W=255.;}
	if (i+1<w)  {if (I[(i+1)+w*j]>0.) I_E= I[(i+1)+w*j]; else I_E=255.;}
	if (j-1>=0) {if (I[i+w*(j-1)]>0.) I_N= I[i+w*(j-1)]; else I_N=255.;}
	if (j+1<h)  {if (I[i+w*(j+1)]>0.) I_S= I[i+w*(j+1)]; else I_S=255.;}
	I_ = MIN(I_S,MIN(I_N,MIN(I_W,I_E)));
	if (I_ == 255) I_ = 0.;
      }

      if (I[ind]>255.){
	if (i-1>=0) {if (I[(i-1)+w*j]<=255.) I_W= I[(i-1)+w*j]; else I_W=0.;}
	if (i+1<w)  {if (I[(i+1)+w*j]<=255.) I_E= I[(i+1)+w*j]; else I_E=0.;}
	if (j-1>=0) {if (I[i+w*(j-1)]<=255.) I_N= I[i+w*(j-1)]; else I_N=0.;}
	if (j+1<h)  {if (I[i+w*(j+1)]<=255.) I_S= I[i+w*(j+1)]; else I_S=0.;}
	I_ = MAX(I_S,MAX(I_N,MAX(I_W,I_E)));
	if (I_ == 0.) I_ = 255.;
      }

      if (I_<0 || I_>255) printf("I_=%f\n", I_);

      mse += (I_ - I0[ind])*(I_ - I0[ind]);
    }

  mse /= (w*h);
  psnr = 10*log10(255*255/mse);

  printf("PSNR = 10 log10(255^2/MSE) = %f db  / RMSE = %2.4f \n", psnr, sqrt(mse));

  return(psnr);

}

/*============================================================================*/





/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : COMP_PSNR_CLIP255_VOIS4_MEAN
_Subject  :

-----------------------------------------------------------------------------*/

float comp_psnr_clip255_vois4_mean(float *I0, int w, int h, float *I) 
{
  float mse=0.;
  float psnr;

  for(int i=0; i<w; i++)
    for(int j=0; j<h; j++){
      int ind  = i+w*j;
      float I_ = I[ind];
      float Np = 1.;

      if (I[ind]<0.){
	if (i-1>=0) if (I[(i-1)+w*j]>0.) {I_ += I[(i-1)+w*j]; Np++;}
	if (i+1<w)  if (I[(i+1)+w*j]>0.) {I_ += I[(i+1)+w*j]; Np++;}
	if (j-1>=0) if (I[i+w*(j-1)]>0.) {I_ += I[i+w*(j-1)]; Np++;}
	if (j+1<h)  if (I[i+w*(j+1)]>0.) {I_ += I[i+w*(j+1)]; Np++;}
	I_ = MAX(0, I_/Np);
      }

      if (I[ind]>255.){
	if (i-1>=0) if (I[(i-1)+w*j]<=255.) {I_ += I[(i-1)+w*j]; Np++;}
	if (i+1<w)  if (I[(i+1)+w*j]<=255.) {I_ += I[(i+1)+w*j]; Np++;}
	if (j-1>=0) if (I[i+w*(j-1)]<=255.) {I_ += I[i+w*(j-1)]; Np++;}
	if (j+1<h)  if (I[i+w*(j+1)]<=255.) {I_ += I[i+w*(j+1)]; Np++;}
	I_ = MIN(255, I_/Np);
      }

      mse += (I_ - I0[ind])*(I_ - I0[ind]);
    }

  mse /= (w*h);
  psnr = 10*log10(255*255/mse);

  printf("PSNR = 10 log10(255^2/MSE) = %f db  / RMSE = %2.4f \n", psnr, sqrt(mse));

  return(psnr);

}

/*============================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : WIENER
_Subject  :

-----------------------------------------------------------------------------*/

void wiener(float *In, int w, int h, float sig, float *I_std, float *Io)
{
  int i;
  float Np=0.;

  for (int i=0; i<w*h; i++){
    float beta = MAX(0., (I_std[i]*I_std[i]-sig*sig)/(I_std[i]*I_std[i]));
    Io[i] = Io[i] + beta*(In[i]-Io[i]);
    if (beta>0.) Np++; 
  }  
  printf("\n   Np(wiener) = %2.0f pixels / %2.2f percent ", Np, 100*Np/(w*h));
}

/*============================================================================*/
