/*=============================================================================
	 
_Header

_Module_Name 	: ByteImage3D.C

_Directory	: 

_Description 	: Declaration de la classe "ByteImage"

_Authors	: Mark HOEBEKE & Charles KERVRANN

_Contents       :

_Last Release   : 24/04/01

_end

=============================================================================*/






/*===========================================================================*/

#ifndef _BYTEIMAGE_H_
#define _BYTEIMAGE_H_

/*===========================================================================*/

#include "Image3D.h"

/*===========================================================================*/


class ByteImage : public Image {
public:
  ByteImage(int, int, int, unsigned char =0);
  ByteImage(int, int, int, unsigned char *);
  ByteImage(int, int, int, char *);
  virtual int get(const int, const int, const int);
  virtual void put(const int, const int, const int, const int);
  virtual unsigned char *get_pixmap(void);
  virtual void set_pixmap(unsigned char *);
  virtual void save(char *);
  virtual int min();
  virtual int max();

  ByteImage(const ByteImage &);
  ByteImage & operator=(const ByteImage &);
  ~ByteImage();

private:
  unsigned char *_pixmap;
};

#endif

/*===========================================================================*/

