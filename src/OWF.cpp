/*=============================================================================

_Header

_Module_Name 	: OWF.cpp

_Directory	: 

_Description 	: Description de OWF.cpp

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 05/02/16

_end

=============================================================================*/





 
/*===========================================================================*/

#include "OWF.h"
#include "include.h"
#include "define.h"
#include "IOmodules/ByteImage3D.h"
#include "MAD.h"
#include "IOmodules/Sauvegarde.h"

#include "CImg.h"

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : QuickSort
_Subject  :

-----------------------------------------------------------------------------*/


void QuickSort(double *a,int left,int right)
{
  int i,j;
  double temp;
  
  i    = left;
  j    = right;
  temp = a[left];
  
  if(left>right) return;
  
  while(i!=j){
    while(a[j]>=temp && j>i)  j--;
    if(j>i)                   a[i++]=a[j];
    
    while(a[i]<=temp && j>i) i++;
    
    if(j>i) a[j--]=a[i];
  }
  
  a[i] = temp;
  QuickSort(a, left, i-1);
  QuickSort(a, i+1, right);
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : WeightsWidth
_Subject  :

-----------------------------------------------------------------------------*/


double  WeightsWidth(double *a, double v, int wd)
{
  int i;
  double ar, cs, cs2, mt;

  //ar = 1.;
  ar = 1e10;
  
  for(cs=0, cs2=v*(v), i=0; i<wd; i++){        
    cs += a[i];
    cs2+= a[i]*(a[i]);
    
    if(cs>0){
      mt = (double)(cs2)/(cs);
      if (mt<=a[i])   break;
      else   	      ar=mt;
    }        
  }

  return(ar);
}

/*===========================================================================*/



  


/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : OWF
_Subject  :

-----------------------------------------------------------------------------*/

void OWF(float *noisy, float *oracle, float *denoisy, float v,  int nw, int np, 
	 int nx, int ny)
{
  double *synoisy, *syoracle;
  int    nwp = nw+np;
  int    nxsy, nysy, nxnysy;
  int    x, y, xp, yp, i, j, k;
  double *cw, *w;
  int    *dadr, *dd;
  int    nw2, np2, nwnw, npnp;
  int    adr, adrp;
  double sum, sum2, dist, dist2, ar, wrho;
  double *rho, *srho;
  float  *variance, *bandwidth;
 


  printf("Patch size (default: 27 x 27): %d x %d \n", 2*np+1, 2*np+1);
  printf("Neighborhood size (default: 13 x 13): %d x %d\n\n", 2*nw+1, 2*nw+1);
 
  nxsy      = nx+2*nwp;
  nysy      = ny+2*nwp;
  nxnysy    = nysy*nxsy;
  synoisy   = new double[nxnysy]; 
  syoracle  = new double[nxnysy]; 
  variance  = new float[nx*ny]; 
  bandwidth = new float[nx*ny];


  // Noise variance estimation
  // -------------------------
  float *residuG = new float[nx*ny];

  //PseudoResidu(noisy,residuG,nx,ny);
  PseudoResidu(oracle,residuG,nx,ny);
  if (v==0.) v = SigmaLTS(residuG,nx*ny);
  printf("Noise standard deviation  : %2.2f\n", v);
  


  // Mirror the image outside the image limits
  // -----------------------------------------  
  for (k=0, j=0;j<nysy; j++){
    if (j<nwp)           y = nwp-j;
    else if (j>ny+nwp-1) y = 2*ny+nwp-j-2;
    else                 y = j-nwp;
    
    for (i=0; i<nxsy; i++, k++){
      if (i<nwp)           x = nwp-i;
      else if (i>nx+nwp-1) x = 2*nx+nwp-i-2;
      else                 x = i-nwp;
      
      synoisy[k]  = noisy[y*nx+x];
      syoracle[k] = oracle[y*nx+x];
    }
  }
  

  
  // Calculate K0(y)
  // ---------------
  nw2   = 2*nw+1;
  nwnw  = nw2*nw2;  
  np2   = 2*np+1;
  npnp  = np2*np2;

  w     = new double[npnp]; 
  dadr  = new int[npnp];    
  cw    = new double[np+1]; 


  
  // Calculate single value of k0(y)
  // -------------------------------
  for(i=1; i<=np; i++){
    for  (cw[i]=0, j=i; j<=np; j++){
      cw[i]+=1./(np*(2*j+1)*(2*j+1));
    }
  }
  cw[0]  = cw[1];


  
  // Calculate k(y) in the patch window
  // ----------------------------------
  for(i=0, y=-np; y<=np; y++)
    for(x=-np; x<=np; x++, i++){
      dadr[i] = y*nxsy+x;
      j = MAX(abs(x), abs(y));
      w[i] = cw[j];     
    }
 


  // Allocation 
  // ----------
  rho        = new double[nwnw]; 
  srho       = new double[nwnw];         
  double arM = 0.;



  // OWF procedure
  // -------------
  for(k=0, y=nwp; y<ny+nwp; y++){
    for(x=nwp; x<nx+nwp; x++, k++){      
      adr = y*nxsy+x;
      
      // Compute the similar function rho(x)
      for (i=0, yp=y-nw; yp<=y+nw; yp++){
	for (xp=x-nw; xp<=x+nw; xp++, i++){
	  adrp = yp*nxsy+xp;
	  	
	  for(j=npnp, dist2=0., dd=dadr; j--; dd++){
	    dist   = (double)syoracle[adr+*dd]-syoracle[adrp+*dd];
	    dist2 += (double)w[j]*dist*dist; 
	  }  
	  
	  rho[i] = srho[i] = MAX((sqrt(dist2)-1.414*v),0.);

	  // oracle computation
	  //dist  = (double)(syoracle[adr]-syoracle[adrp]);
	  //rho[i] = srho[i] = fabs(dist);
	}
      }
    
      // Calculate a
      QuickSort(srho,0,nwnw-1);
      ar = WeightsWidth(srho, v, nwnw);
      if (ar > arM) arM = ar;
      
      // Caculate estimator
      for (dist=0., sum=0., sum2=0., i=0, yp=y-nw; yp<=y+nw; yp++){
	for (xp=x-nw; xp<=x+nw; xp++, i++){

	  dist2  = (double)(ar-rho[i]);
	  wrho   =  MAX(dist2, 0.);

	  dist  += (double)(wrho*synoisy[yp*nxsy+xp]);
	  sum   += (double)(wrho);
	  sum2  += (double)(wrho*wrho);
	}
      }

      // Save
      denoisy[k]   = (float)(dist/sum);
      bandwidth[k] = (float)MIN(MAX(ar,0),255); 
      variance[k]  = v*v*sum2/(sum*sum);
    }
  }
    


  // Save the results
  // ----------------
  float *sy   = new float[nxnysy];
  for (i=0; i<nxnysy; i++) sy[i] = synoisy[i];



  // Statistics
  // ----------
  float armean  = 0.;
  float armed   = 0.;
  float armax   = 0.;
  int Nmed      = (int)(nx*ny/2.);
  double *artab = new double[nx*ny]; 

  for (k=0; k<nx*ny; k++){
    if (bandwidth[k]>armax) 
      armax = bandwidth[k];
    armean  += bandwidth[k];
    artab[k] = (double)(bandwidth[k]);
  }
  armean /= (nx*ny);
  QuickSort(artab,0,nx*ny-1);
  //printf("\n   Bandwidth statistics: armean = %f armed = %f arM=%f ar_max=%f\n\n", armean, artab[Nmed], armax, arM);
}

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : OWF_oracle
_Subject  :

-----------------------------------------------------------------------------*/

void OWF_oracle(float *noisy, float *oracle, float *denoisy, float v,  int nw, 
		int nx, int ny)
{
  double *synoisy, *syoracle;
  int    np  = 0;
  int    nwp = nw+np;
  int    nxsy, nysy, nxnysy;
  int    x, y, xp, yp, i, j, k;
  double *cw, *w;
  int    *dadr, *dd;
  int    nw2, np2, nwnw, npnp;
  int    adr, adrp;
  double sum, sum2, dist, dist2, ar, wrho;
  double *rho, *srho;
  float  *variance, *bandwidth;

  
  printf("\nOWF oracle procedure\n");
  printf("Neighborhood size: %d x %d \n\n", 2*nw+1, 2*nw+1);

  nxsy      = nx+2*nwp;
  nysy      = ny+2*nwp;
  nxnysy    = nysy*nxsy;
  synoisy   = new double[nxnysy]; 
  syoracle  = new double[nxnysy]; 
  variance  = new float[nx*ny];  
  bandwidth = new float[nx*ny];


  // Noise variance estimation
  // -------------------------
  float *residuG  = new float[nx*ny];
  PseudoResidu(oracle,residuG,nx,ny);
  if (v==0.) v = SigmaLTS(residuG,nx*ny);
 


  // Mirror the image outside the image limits
  // -----------------------------------------  
  for (k=0, j=0;j<nysy; j++){
    if (j<nwp)           y = nwp-j;
    else if (j>ny+nwp-1) y = 2*ny+nwp-j-2;
    else                 y = j-nwp;
    
    for (i=0; i<nxsy; i++, k++){
      if (i<nwp)           x = nwp-i;
      else if (i>nx+nwp-1) x = 2*nx+nwp-i-2;
      else                 x = i-nwp;
      
      synoisy[k]  = noisy[y*nx+x];
      syoracle[k] = oracle[y*nx+x];
    }
  }
  

  
  // Calculate K0(y)
  // ---------------
  nw2   = 2*nw+1;
  nwnw  = nw2*nw2;  
  np2   = 2*np+1;
  npnp  = np2*np2;

  w     = new double[npnp]; 
  dadr  = new int[npnp];    
  cw    = new double[np+1]; 


  
  // Calculate single value of k0(y)
  // -------------------------------
  for(i=1; i<=np; i++){
    for  (cw[i]=0, j=i; j<=np; j++){
      cw[i]+=1./(np*(2*j+1)*(2*j+1));
    }
  }
  cw[0]  = cw[1];


  
  // Calculate k(y) in the patch window
  // ----------------------------------
  for(i=0, y=-np; y<=np; y++)
    for(x=-np; x<=np; x++, i++){
      dadr[i] = y*nxsy+x;
      j = MAX(abs(x), abs(y));
      w[i] = cw[j];
    }



  // Allocation 
  // ----------
  rho  = new double[nwnw]; 
  srho = new double[nwnw]; 
      


  // OWF oracle procedure
  // -------------------
  for(k=0, y=nwp; y<ny+nwp; y++){
    for(x=nwp; x<nx+nwp; x++, k++){      
      adr = y*nxsy+x;
      
      // Compute the similar function rho(x)
      for (i=0, yp=y-nw; yp<=y+nw; yp++){
	for (xp=x-nw; xp<=x+nw; xp++, i++){
	  adrp = yp*nxsy+xp;
	  	
	  dist  = (double)(syoracle[adr]-syoracle[adrp]);
	  rho[i] = srho[i] = fabs(dist);
	}
      }
    
      // Calculate a
      QuickSort(srho,0,nwnw-1);
      ar = WeightsWidth(srho, v, nwnw);
      
      // Caculate estimator
      for (dist=0., sum=0., sum2=0., i=0, yp=y-nw; yp<=y+nw; yp++){
	for (xp=x-nw; xp<=x+nw; xp++, i++){

	  dist2  = (double)(ar-rho[i]);
	  wrho   = MAX(dist2, 0.);

	  dist  += (double)wrho*synoisy[yp*nxsy+xp];
	  sum   += (double)wrho;
	  sum2  += (double)(wrho*wrho);
	}
      }

      // Save
      denoisy[k]   = (float)(dist/sum);
      bandwidth[k] = (float)MIN(MAX(ar,0),255); 
      variance[k]  = v*v*sum2/(sum*sum);
    }
  }
}

/*===========================================================================*/







