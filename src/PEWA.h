/*=============================================================================

_Header

_Module_Name 	:  PEWA.h

_Directory	: 

_Description 	: Declaration de  PEWA.cpp

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 12/06/14

_end

=============================================================================*/





/*===========================================================================*/

#ifndef _PEWA_H_
#define _PEWA_H_

/*===========================================================================*/

#include "define.h"
#include "include.h"
#include "CImg.h"
 
using namespace cimg_library;
// The undef below is necessary when using a non-standard compiler.
#ifdef cimg_use_visualcpp6
#define std  
#endif

/*===========================================================================*/

extern void  PEWA(float *oracle, float *_pixmap, int w, int h, float SigmaManuel, 
					int TailleMotifs, int TailleVois, float *I_out);

/*===========================================================================*/

#endif

/*===========================================================================*/

