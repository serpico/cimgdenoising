/*=============================================================================

   copyright(c) 2003-2004 by INRA-Biometrie Jouy-en-Josas.
	                  All Rights Reserved.

-------------------------------------------------------------------------------

_Header

_Module_Name 	: SAFIR.h

_Directory	:

_Description 	: Declaration de SAFIR.cc

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 29/10/18

_end

=============================================================================*/





/*===========================================================================*/

#ifndef _SAFIR_H_
#define _SAFIR_H_

/*===========================================================================*/

#include "define.h"
#include "include.h"
#include "CImg.h"
 
using namespace cimg_library;
// The undef below is necessary when using a non-standard compiler.
#ifdef cimg_use_visualcpp6
#define std  
#endif


/*===========================================================================*/

extern float table_chi2 (float p, int dof);
extern float moyenne(float *X, int N);
extern float variance(float *X, int N);
extern float correlation(float *X, float *Y, int N);
extern int   condition_total_exactitude(float *X, float *Y, int N);
extern int   condition_correlation(float *I, float *f, int N, float *c_);
extern float I_divergence(float *f, float *f_, int N);
extern void  initialisation_patches(float *_pixmap, float *sig_I, float *f_set,
				    float *Var_set, int N);
extern void  SAFIR(float *_pixmap, int w, int h, float SigmaManuel, int TaillePatches, int ITER, float *f_star);

/*===========================================================================*/

#endif

/*===========================================================================*/

