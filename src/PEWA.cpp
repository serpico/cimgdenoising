/*=============================================================================

_Header

_Module_Name 	: PEWA.cpp

_Directory	: 

_Description 	: Description de PEWA.cpp

_Calls   	:

_Authors	: Charles KERVRANN

_Contents       :

_Last Release   : 12/06/14

_end

=============================================================================*/




/*===========================================================================*/

#define Nmcmc 1000
 
/*===========================================================================*/

#include "include.h"
#include "define.h"
#include "MAD.h"
#include "PEWA.h"
#include "DCTmodules/DCTdenoising.h"

#include "CImg.h"

/*===========================================================================*/






/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : Patch-based EWA 
_Subject  :

-----------------------------------------------------------------------------*/

void  PEWA(float *oracle, float *_pixmap, int w, int h, float SigmaManuel, 
				int TailleMotifs, int TailleVois, float *I_out)
{
  int    i_, j_, i, j, k, l;
  int    ind, ind_w;
  int    dimP = (2*TailleMotifs+1);
  int    i_save, j_save, vrand_save; 
  int    vrand_;
  int    SpatialSampling = 1;
  int    dynMH = 1; 
  float  sig, sig2;
  float  n;
  float  beta   = 4.;
  float  ICmean = 3.;
  float  ICvar  = 1.65;
  float  acut = 1.;
  double Pr;
  double E_save;




  // ALLOCATION DYNAMIQUE 
  // ==================== 
  double *f_star   = new double[w*h];
  double *f_star2  = new double[w*h];
  double *w_star   = new double[w*h];
  double *f_save   = new double[dimP*dimP];
  double *f_patch  = new double[dimP*dimP];
  double *w_patch  = new double[dimP*dimP];
  double *w_save   = new double[dimP*dimP];
  float *residuG   = new float[w*h];
  float *noise     = new float[w*h];
  float *I_        = new float[w*h];
  float *I_pixmap  = new float[w*h];
  float *f_init    = new float[w*h];

  


  // CALCULATE SPATIAL WEIGHTS 
  // =========================
  int x,y;
  int np = TailleMotifs;
  int npnp= (2*np+1)*(2*np+1);
  double *d2m     = new double[npnp];
  double *weight  = new double[npnp]; 
  double *cw = new double[np+1]; 

  for(i=1; i<=np; i++){
    for  (cw[i]=0, j=i; j<=np; j++){
      cw[i]+=1./(np*(2*j+1)*(2*j+1));
    }
  }
  cw[0]  = cw[1];  
 
  for(i=0, y=-np; y<=np; y++){
    for(x=-np; x<=np; x++, i++){
      j = MAX(abs(x), abs(y));
      weight[i] = cw[j];
    }
  }
 



  // ETAPES D'INITIALISATION DE L'ALGORITHME 
  // ======================================= 
 
  // Estimation de la variance du bruit 
  // ---------------------------------- 
  PseudoResidu(_pixmap,residuG,w,h);
  if (SigmaManuel==0.) sig = SigmaLTS(residuG,w*h);
  else                 sig = SigmaManuel; 
  sig2 = sig*sig;


  // Tailles de patch et de voisinage 
  // -------------------------------- 
  int du       = TailleMotifs;
  int dv       = TailleMotifs;
  int delta    = TailleVois;
  int delta0   = delta;
  float Np     = (2*du+1)*(2*dv+1); 
  float Nvois  = (float)((2*delta+1)*(2*delta+1));
  float Nvois0 = Nvois;
  float Varp   = delta0*delta0;

  printf("\nNoise sigma : %2.2f\n", sig);
  printf("Patch size (default: 5 x 5 or 7 x 7): %d x %d\n", 2*TailleMotifs+1,  2*TailleMotifs+1);
  printf("Neighborhood size (default (Step 2): 15 x 15): %d x %d\n\n", 2*delta+1,  2*delta+1);


  // Estimation des moyennes et variances locales sur les blocs 
  // ---------------------------------------------------------- 
  float SupV   = ICvar; 
  float *npix  = new float[w*h];
  float *SupM  = new float[w*h];
  float *Imean = new float[w*h];
  float *Ivar  = new float[w*h];  
  float *Idata = new float[w*h];  
 
#pragma omp parallel for private(i)
  for (i=0; i<w*h; i++){
    I_pixmap[i] = _pixmap[i];
    Idata[i]    = oracle[i];
    npix[i]     = 0.;
    Imean[i]    = 0.;
    Ivar[i]     = 0.;
  }
 
#pragma omp parallel for private(i)
  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      ind=i+w*j;
      for (int u=-du; u<=du; u++)
	for (int v=-dv; v<=dv; v++){
	  if (INCLUS(i+u,j+v,w,h)!=0){
	    Imean[ind] += Idata[i+u+w*(j+v)];
	    npix[ind]+=1.; 
	  }
	}
      Imean[ind] /= npix[ind]; 
      SupM[ind]   = (ICmean*sig)/sqrt(npix[ind]);
    }
 
#pragma omp parallel for private(i)
  for(i=0; i<w; i++)
    for(j=0; j<h; j++){
      ind=i+w*j;
      for (int u=-du; u<=du; u++)
	for (int v=-dv; v<=dv; v++)
	  if (INCLUS(i+u,j+v,w,h)!=0) Ivar[ind] += (Idata[i+u+w*(j+v)]-Imean[ind])*(Idata[i+u+w*(j+v)]-Imean[ind]);
      Ivar[ind] /= npix[ind];   
      Ivar[ind] = MAX(Ivar[ind], 1e-5);
    }
 
  
  // Computation of competing DCT estimators
  //----------------------------------------
  int Nb       = 5;
  float s0     = sig/4.;
  float start  = -s0; 
  start        = s0;

  std::vector< vector< float > > DCTn;
  DCTn.resize(Nb);
  for (int i=0; i<Nb; i++) DCTn[i].resize(w*h); 
  for (int i=0; i<Nb; i++) for (int j=0; j<w*h; j++) DCTn[i][j] = 0.; 
  float *DCT_ = new float[w*h];
  
  for (int j=0; j<w*h; j++) DCTn[Nb-2][j] = _pixmap[j];
  
  for (int i=0; i<Nb-2; i++){
    DCTdenoising(_pixmap,DCT_,w,h,1,(sig+i*s0+start),16);
    for (int j=0; j<w*h; j++) DCTn[i][j] =  DCT_[j];
  }
  
  for (int j=0; j<w*h; j++) DCTn[Nb-1][j] = oracle[j];


  

  // PROCEDURE DE DEBRUITAGE 
  // ======================= 
  int vrandM = Nb;
    
  for(i=0; i<w*h; i++){
    f_init[i]  = oracle[i]; 
    f_star[i]  = oracle[i];
    w_star[i]  = 1.;      
    f_star2[i] = oracle[i]*oracle[i];
  }
  

  // Estimation par blocs 
  // -------------------- 
  for(i=0; i<w; i+=SpatialSampling){
    for(j=0; j<h; j+=SpatialSampling){
      ind     = i+w*j;
      Nvois   = Nvois0;
      
      i_save  = i;
      j_save  = j;
            
      double grandx, grandy;
      float npoints  = 0.;
      
      double Eold  = 0.;
      double Eold0 = 0.;
      
      Np=0.;
 
      for (int u=-du; u<=du; u++) 
	for (int v=-dv; v<=dv; v++){
	  int p  = i+u;
	  int q  = j+v;
	  
	  if (INCLUS(p,q,w,h)!=0){
	    int pq = p+w*q;

	    int pos = u+du+(2*np+1)*(v+dv);
	    Eold += (_pixmap[pq]-f_init[pq])*(_pixmap[pq]-f_init[pq])*weight[pos];

	    f_save[(u+du)*dimP+v+dv] = f_init[pq];
	    w_save[(u+du)*dimP+v+dv] = 1.;
	    Np++;
	  }
	}

      Eold0 = Eold;  
      Eold  = fabs((Eold0 - Np*sig2)/(beta*sig2));      	    
      Eold  =  MAX(4*Np/4.,Eold);

      
      float NT    = 0.;
      float gamma = 1.;
      while (npoints<acut*Nmcmc){
	int flag = 0;	    	  
	if (flag==0){	      
	  int x1     = rand();
	  int x2     = rand();
	  double x1_ = (float)(x1)/(float)(RAND_MAX);
	  double x2_ = (float)(x2)/(float)(RAND_MAX);
	  grandx     = gamma*delta*sqrt(-2*log((double)(1e-10 + (1-2e-10)*x1_)))*cos((double)(2*PI*x2_));	  
	  int y1     = rand();
	  int y2     = rand();
	  double y1_ = (float)(y1)/(float)(RAND_MAX);
	  double y2_ = (float)(y2)/(float)(RAND_MAX);
	  grandy     = gamma*delta*sqrt(-2*log((double)(1e-10 + (1-2e-10)*y1_)))*cos((double)(2*PI*y2_));	  
	}
	       
	npoints +=1;
	i_       = (int)(i_save-grandx+0.5);
	j_       = (int)(j_save-grandy+0.5);
	vrand_   = rand() % vrandM + 0;

	double Enew = 0.;
	double df   = 0.;
	if (INCLUS(i_,j_,w,h)!=0){ 
	  ind_w=i_+w*j_;
	  
	  if ((fabs(Imean[ind_w]-Imean[ind])<SupM[ind]) && ((MAX(Ivar[ind_w],Ivar[ind])/MIN(Ivar[ind_w],Ivar[ind]))<SupV)){
	    Np=0;

	    for (int u=-du; u<=du; u++) 
	      for (int v=-dv; v<=dv; v++){
		int p  = i+u;
		int q  = j+v;
		int p_ = i_+u;
		int q_ = j_+v;
		
		if ((INCLUS(p,q,w,h)!=0) && (INCLUS(p_,q_,w,h)!=0)){
		  int pq = p+w*q; 
		  int p_q_ = p_+w*q_;
		  
		  f_patch[(u+du)*dimP+v+dv]  =  DCTn[vrand_][p_q_];

		  int pos = u+du+(2*np+1)*(v+dv);
		  Enew += (_pixmap[pq]-DCTn[vrand_][p_q_])*(_pixmap[pq]-DCTn[vrand_][p_q_]);

		  Np += 1.;
		}
	      }

	    double Enew0 = Enew;
	    Enew         = fabs(Enew0 - Np*sig2)/(beta*sig2);
	    double Enew_ = Enew;	    
	    double dU    = (Enew - Eold);

	    float a       = (float)(rand())/(float)(RAND_MAX);	    
	    float d_save  = (float)(((i_save-i)*(i_save-i)+(j_save-j)*(j_save-j))/(float)(2*Varp));
	    float d_      = (float)(((i_-i)*(i_-i)+(j_-j)*(j_-j))/(float)(2*Varp));
	    float dU_pos  = d_ - d_save;
	    float DU      = dU+dU_pos; 
	    
	    if (Enew0>0.){	 
	      Pr = exp(-DU);
	      if (a<(MIN(1.0+exp(-DU),1.0+exp(DU))/(1.0+exp(DU)))){
		Eold = Enew;		
		
		for (int u=-du; u<=du; u++)
		  for (int v=-dv; v<=dv; v++){
		    int p_   = i_+u;
		    int q_   = j_+v;
		    int p_q_ = p_+w*q_;		    
		    if (INCLUS(p_,q_,w,h)!=0){

		      f_save[(u+du)*dimP+v+dv] = f_patch[(u+du)*dimP+v+dv];
		      w_save[(u+du)*dimP+v+dv] = w_patch[(u+du)*dimP+v+dv];
		    }
		  }

		E_save     = Enew;
	        vrand_save = vrand_;
		if (npoints<=Nvois) NT+=1; 
		    
		if (npoints>Nvois){
		  i_save = i_;
		  j_save = j_;
		}
	      }
	      else if (npoints>Nvois) gamma = 1.;
	    }  
	  }
	      
	  if (npoints==Nvois) if (NT/Nvois<0.025) gamma=2.;

	  if (npoints>Nvois){
	    for (int u=-du; u<=du; u++)
	    for (int v=-dv; v<=dv; v++){
	
		int p  = i+u;
		int q  = j+v;
		int pq = p+w*q;
		
		if (INCLUS(p,q,w,h)!=0){ 
		  if (dynMH==0){
		    f_star2[pq] += f_save[(u+du)*dimP+v+dv]*f_save[(u+du)*dimP+v+dv];
		    f_star[pq]  += f_save[(u+du)*dimP+v+dv];
		    w_star[pq]  += 1.; 
		  }
		  else{
		    f_star[pq] += (NT/Nvois)*f_save[(u+du)*dimP+v+dv];
		    w_star[pq] += (NT/Nvois);
		  }
		}
	      }
	  }
	}
      }
    }
  }
  
  


  // SAUVEGARDE DES RESULTATS 
  // ======================== 
  for(i=0; i<w*h; i++)  I_out[i] = (float)(f_star[i]/w_star[i]);  
}

/*=============================================================================*/




















