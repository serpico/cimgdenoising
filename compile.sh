#!/bin/sh

# build
cd $(dirname "$0")
mkdir build
cd build
cmake ..
make
cd ..

# run tests
cd build/tests
ctest

#install
cd ..
dir=$(pwd)
#ln -s $dir/bin/denoise /usr/bin/denoise
mv $dir/bin/denoise /usr/local/bin/denoise