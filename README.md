## CImg Denoising

c++ library based on CImg for image denoising
This library only prcess 2D gray scaled images. If a 3D image or color images is given, it will process all the layers or channels independently

## Compile

mkdir build <br/>
cd build <br/>
cmake .. <br/>
make <br/>

## Usage

denoise -i ../data/Montage.pgm -o ../data/Test -msg 20.0 -algo SV -denoisep 1.85 -sparsep 0.75 <br/>
denoise -i ../data/Montage.pgm -o ../data/Test -msg 20.0 -algo NLBayes <br/>
denoise -i ../data/Montage.pgm -o ../data/Test -msg 20.0 -algo BM3D <br/>
denoise -i ../data/Montage.pgm -o ../data/Test -msg 20.0 -algo SAFIR <br/>
 <br/>
denoise -i ../data/Montage.pgm -o ../data/Test -ng 20.0 -algo SV -denoisep 4. -stab -sparsep 0.5 <br/>
denoise -i ../data/Montage.pgm -o ../data/Test -ng 20.0 -algo SV -denoisep 4. -stab -sparsep 0.4 <br/>

## Singularity

Build `denoise` in a singularity image

```bash
cd ..
sudo singularity build --sandbox serpicodenoise cimgdenoising/Singularity

```

Run the container

```bash
 sudo singularity shell --writable serpicodenoise
```